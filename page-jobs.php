
<?php
/**
 * Template Name: Jobs
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'page-jobs'); ?>
<?php endwhile; ?>
