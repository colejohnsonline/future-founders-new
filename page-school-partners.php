<?php
/**
 * Template Name: School Partners
 */
?>
<?php while (have_posts()) : the_post(); ?>
  <div class="container-static">
  	<?php get_template_part('templates/page', 'header'); ?>
  </div>
  <?php get_template_part('templates/content', 'page-school-partners'); ?>
<?php endwhile; ?>

