<?php

/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = ['lib/utils.php',

// Utility functions
'lib/init.php',

// Initial theme setup and constants
'lib/wrapper.php',

// Theme wrapper class
'lib/conditional-tag-check.php',

// ConditionalTagCheck class
'lib/config.php',

// Configuration
'lib/assets.php',

// Scripts and stylesheets
'lib/titles.php',

// Page titles
'lib/nav.php',

// Custom nav modifications
'lib/gallery.php',

// Custom [gallery] modifications
'lib/extras.php',

// Custom functions
];
foreach ($sage_includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
    }
    require_once $filepath;
}
unset($file, $filepath);
add_action('admin_print_styles', 'custom_editor_style');
function custom_editor_style() {
    
    // Get current screen and determine if we are using the editor
    $screen = get_current_screen();
    
    // If we are editing (adding new) post or page
    if ($screen->post_type == 'page' || $screen->post_type == 'post' || $screen->post_type == 'news-article' || $screen->post_type == 'job' || $screen->post_type == 'event') {
        
        // Register our stylesheet
        // Note: The stylesheet resides in our plugin directory/css/admin.css
        // You may change this to suit your preferences
        wp_register_style('custom-editor-style', ('/wp-content/themes/futurefounders-new/custom-editor-style.css'), array());
        
        // Now we enqueue our stylesheet
        wp_enqueue_style('custom-editor-style');
    }
}
function acf_event_date_func($atts) {
    $date = get_field('event_date');
    $date_timestamp = strtotime($date);
    $date_formatted = date("F j, Y", $date_timestamp);
    return $date_formatted;
}
add_shortcode('acf_event_date', 'acf_event_date_func');

function acf_due_date_func($atts) {
    $date = get_field('due');
    $date_timestamp = strtotime($date);
    $date_formatted = date("F j, Y", $date_timestamp);
    return $date_formatted;
}
add_shortcode('acf_due_date', 'acf_due_date_func');

add_filter("gform_init_scripts_footer", "init_scripts");
function init_scripts() {
    return true;
}

add_shortcode('curated_flag', 'curated_flag_func');
function curated_flag_func($atts) {
    $is_curated = get_field('future_founders_verified', $post->ID);
    
    //echo $is_curated;
    if ($is_curated[0] == 'verified'):
        return '<span class="glyphicon glyphicon-star pull-right" aria-hidden="true"></span>';
    else:
        return;
    endif;
}

/*
add_filter('wpv_filter_query_post_process', 'remove_past_events', 99, 324);
function remove_past_events($query, $view_settings, $view_id) {
$events = new WP_Query(
array(
'post_type' => 'event', // Tell WordPress which post type we want
'orderby' => 'meta_value', // We want to organize the events by date
'meta_key' => 'event_date', // Grab the "start date" field created via "More Fields" plugin (stored in YYYY-MM-DD format)
'order' => 'ASC', // ASC is the other option
'posts_per_page' => '-1', // Let's show them all.
'meta_query' => array( // WordPress has all the results, now, return only the events after today's date
array(
'key' => 'event_date', // Check the start date field
'value' => current_time('Ymd'), // Set today's date (note the similar format)
'compare' => '>=', // Return the ones greater than today's date
'type' => 'NUMERIC,' // Let WordPress know we're working with numbers
)
)
)
);
wp_reset_postdata();
return $events;
}
*/
add_filter('wpv_filter_query', 'remove_past_events', 100, 3);
function remove_past_events($query_args, $view_settings, $view_id) {
    
    $types = (array)$query_args['post_type'];
    if (!is_admin() && in_array('event', $types) && is_page('95')) {
        $query_args['meta_query'] = array(array('key' => 'event_date',
        
        // Check the start date field
        'value' => current_time('Ymd'),
        
        // Set today's date (note the similar format)
        'compare' => '>=',
        
        // Return the ones greater than today's date
        'type' => 'NUMERIC,'
        
        // Let WordPress know we're working with numbers
        ), array('key' => 'startup_event', 'value' => '1', 'compare' => '==', 'type' => 'STRING'));
    }
    if (!is_admin() && in_array('event', $types) && is_page('5467')) {
        
        $query_args['meta_query'] = array(array('key' => 'event_date',
        
        // Check the start date field
        'value' => current_time('Ymd'),
        
        // Set today's date (note the similar format)
        'compare' => '>=',
        
        // Return the ones greater than today's date
        'type' => 'NUMERIC,'
        
        // Let WordPress know we're working with numbers
        ), array('key' => 'future_founders_event', 'value' => '1', 'compare' => '==', 'type' => 'STRING'));
    }
    
    // startup featured events
    /*$filtered_posts = get_view_query_results('5651');
    $exclude_these = array();
    foreach ($filtered_posts as $filtered_post) {
    $exclude_these[] = $filtered_post->ID;
    }*/
    if (!is_admin() && in_array('event', $types)) {
        if ($view_id == 324) {
            $query_args['meta_query'] = array(array('key' => 'event_date',
            
            // Check the start date field
            'value' => current_time('Ymd'),
            
            // Set today's date (note the similar format)
            'compare' => '>=',
            
            // Return the ones greater than today's date
            'type' => 'NUMERIC,'
            
            // Let WordPress know we're working with numbers
            ), array('key' => 'startup_event', 'value' => '1', 'compare' => '==', 'type' => 'STRING'), 'posts_per_page' => 3);
        }
        if ($view_id == 5651) {
            $query_args['meta_query'] = array(array('key' => 'event_date',
            
            // Check the start date field
            'value' => current_time('Ymd'),
            
            // Set today's date (note the similar format)
            'compare' => '>=',
            
            // Return the ones greater than today's date
            'type' => 'NUMERIC,'
            
            // Let WordPress know we're working with numbers
            ), array('key' => 'startup_event', 'value' => '1', 'compare' => '==', 'type' => 'STRING'), 'posts_per_page' => 3, 'offset' => 3);
        }
        if ($view_id == 5649) {
            $query_args['meta_query'] = array(array('key' => 'event_date',
            
            // Check the start date field
            'value' => current_time('Ymd'),
            
            // Set today's date (note the similar format)
            'compare' => '>=',
            
            // Return the ones greater than today's date
            'type' => 'NUMERIC,'
            
            // Let WordPress know we're working with numbers
            ), array('key' => 'startup_event', 'value' => '1', 'compare' => '==', 'type' => 'STRING'), 'posts_per_page' => 3, 'offset' => 3);
        }
    }
    
    /* jobs */
    
    if (!is_admin() && in_array('job', $types)) {
        if ($view_id == 325) {
            $query_args['meta_query'] = array(array('key' => 'due',
            
            // Check the start date field
            'value' => current_time('Ymd'),
            
            // Set today's date (note the similar format)
            'compare' => '>=',
            
            // Return the ones greater than today's date
            'type' => 'NUMERIC,'
            
            // Let WordPress know we're working with numbers
            ), 'posts_per_page' => 3);
        }
        if ($view_id == 5650) {
            $query_args['meta_query'] = array(array('key' => 'due',
            
            // Check the start date field
            'value' => current_time('Ymd'),
            
            // Set today's date (note the similar format)
            'compare' => '>=',
            
            // Return the ones greater than today's date
            'type' => 'NUMERIC,'
            
            // Let WordPress know we're working with numbers
            ), 'posts_per_page' => 3);
        }
    }
    return $query_args;
}
function get_tags_func($atts) {
    $all_tags = get_field("industry");
    
    $post_type = get_post_type(get_the_ID());
    ob_start();
    
    if ($post_type == 'news-article') {
        if (is_page(124)) {
            if ($all_tags) {
                $i = 0;
                foreach ($all_tags as $tag) {
                    if ($i < 3) { ?>
<a style="font-size: 12px;" href="<?php
                        echo site_url(); ?>/startup/news/?industry=<?php
                        echo $tag; ?>"><?php
                        echo '#' . $tag . '&nbsp;' ?></a>
<?php
                        $i++;
                    }
                }
            }
        }
        
        if (is_page(3492)) {
            $posttags = get_the_tags();
            if ($posttags) {
                $i = 0;
                foreach ($posttags as $tag) {
                    if ($i < 3) {
                        echo '&nbsp;#<a style="font-size: 12px;" href="http://futurefounders.com/tag/' . preg_replace('#[ -]+#', '-', $tag->name) . '">' . $tag->name . '</a> ';
                        $i++;
                    }
                }
            }
        }
    }
    
    if ($post_type == 'event') {
        if (is_page(95)) {
            if ($all_tags) {
                $i = 0;
                foreach ($all_tags as $tag) {
                    if ($i < 3) { ?>
<a style="font-size: 12px;" href="<?php
                        echo site_url(); ?>/startup/events/?industry=<?php
                        echo $tag; ?>"><?php
                        echo '#' . $tag . '&nbsp;' ?></a>
<?php
                        $i++;
                    }
                }
            }
        }
        if (is_page(5467)) {
            $posttags = get_the_tags();
            if ($posttags) {
                $i = 0;
                foreach ($posttags as $tag) {
                    if ($i < 3) {
                        echo '&nbsp;#<a style="font-size: 12px;" href="http://futurefounders.com/tag/' . preg_replace('#[ -]+#', '-', $tag->name) . '">' . $tag->name . '</a> ';
                        $i++;
                    }
                }
            }
        }
    }
    
    if ($post_type == 'job') {
        if ($all_tags) {
            $i = 0;
            foreach ($all_tags as $tag) {
                if ($i < 3) { ?>
<a style="font-size: 12px;" href="<?php
                    echo site_url(); ?>/startup/jobs/?industry=<?php
                    echo $tag; ?>"><?php
                    echo '#' . $tag . '&nbsp;' ?></a>
<?php
                    $i++;
                }
            }
        }
    }
    
    return ob_get_clean();
}
add_shortcode('get_tags', 'get_tags_func');
add_filter('query_vars', 'add_my_var');
function add_my_var($public_query_vars) {
    $public_query_vars[] = 'from_date';
    $public_query_vars[] = 'to_date';
    $public_query_vars[] = 'type';
    $public_query_vars[] = 'grade_level';
    $public_query_vars[] = 'session';
    $public_query_vars[] = 'industry';
    $public_query_vars[] = 'interests';
    $public_query_vars[] = 'curated';
    $public_query_vars[] = 'notcurated';
    $public_query_vars[] = 'location';
    $public_query_vars[] = 'company';
    $public_query_vars[] = 'jobtype';
    $public_query_vars[] = 'paid';
    return $public_query_vars;
}

add_filter('wpv_filter_query_post_process', 'news_filter', 100, 3);
function news_filter($query, $view_settings, $view_id) {
    
    if ($view_id == 323) {
        $param_fromdate = get_query_var('from_date', '0');
        $param_todate = get_query_var('to_date', '0');
        $param_industry = get_query_var('industry', 'All');
        $param_interest = get_query_var('interests', 'All');
        $param_curated = get_query_var('curated', 'notcurated');
        $param_notcurated = get_query_var('notcurated', 'curated');
        $new_posts = array();
        
        $date_timestamp_from = strtotime($param_fromdate);
        $date_from = date("F j, Y", $date_timestamp_from);
        
        $date_timestamp_to = strtotime($param_todate);
        $date_to = date("F j, Y", $date_timestamp_to);
        
        foreach ($query->posts as $post) {
            $acf_industries = get_field('industry', $post->ID);
            $acf_interests = get_field('interests', $post->ID);
            $acf_curated = get_field('future_founders_verified', $post->ID);
            $acf_date = strtotime(get_the_date("F j, Y", $post->ID));
            if ($acf_curated == '') {
                $acf_curated = 1;
            }
            if (($date_timestamp_from < $acf_date && $date_timestamp_to > $acf_date) || ($date_timestamp_from < $acf_date && $param_todate == 0) || ($date_timestamp_to > $acf_date && $param_fromdate == 0) || ($param_fromdate == 0 && $param_todate == 0)) {
                if (!is_array($acf_industries) || !is_array($acf_interests)) {
                } 
                elseif ((is_array($acf_industries) && in_array($param_industry, $acf_industries)) || $param_industry == 'All') {
                    if ((is_array($acf_interests) && in_array($param_interest, $acf_interests)) || $param_interest == 'All') {
                        
                        if ($acf_curated[0] == 'verified' && $param_curated == 'verified') {
                            $new_posts[] = $post;
                        } 
                        elseif ($param_notcurated = 'notverified' && $param_curated == 'notcurated' && $acf_curated == 1 || $acf_curated[0] == 'verified') {
                            $new_posts[] = $post;
                        }
                    }
                }
            }
        }
        
        if (count($new_posts) > 0):
            $query->posts = $new_posts;
        else:
            echo '<h3 class="blue light">No opportunities available at this time, please check back later.</h3>';
            $query->posts = array();
        endif;
    }
    
    wp_reset_query();
    return $query;
}
add_filter('wpv_filter_query_post_process', 'events_filter', 100, 3);
function events_filter($query, $view_settings, $view_id) {
    
    if ($view_id == 324) {
        $param_fromdate = get_query_var('from_date', '0');
        $param_todate = get_query_var('to_date', '0');
        $param_industry = get_query_var('industry', 'All');
        $param_interest = get_query_var('interests', 'All');
        $param_location = get_query_var('location', 'location');
        $param_curated = get_query_var('curated', 'notcurated');
        $param_notcurated = get_query_var('notcurated', 'curated');
        $new_posts = array();
        echo $param_location;
        $date_timestamp_from = strtotime($param_fromdate);
        $date_from = date("F j, Y", $date_timestamp_from);
        
        $date_timestamp_to = strtotime($param_todate);
        $date_to = date("F j, Y", $date_timestamp_to);
        
        foreach ($query->posts as $post) {
            $acf_industries = get_field('industry', $post->ID);
            $acf_interests = get_field('interests', $post->ID);
            $acf_location = get_field('location_name', $post->ID);
            $acf_curated = get_field('future_founders_verified', $post->ID);
            $acf_date = strtotime(get_field('event_date', $post->ID));
            if ($acf_curated == '') {
                $acf_curated = 1;
            }
            if (($date_timestamp_from < $acf_date && $date_timestamp_to > $acf_date) || ($date_timestamp_from < $acf_date && $param_todate == 0) || ($date_timestamp_to > $acf_date && $param_fromdate == 0) || ($param_fromdate == 0 && $param_todate == 0)) {
                
                if ((is_array($acf_industries) && in_array($param_industry, $acf_industries)) || $param_industry == 'All') {
                    if ((is_array($acf_interests) && in_array($param_interest, $acf_interests)) || $param_interest == 'All') {
                        if (strcasecmp($param_location, $acf_location) == 0 || $param_location == 'location' || $param_location == '') {
                            if ($acf_curated[0] == 'verified' && $param_curated == 'verified') {
                                $new_posts[] = $post;
                            } 
                            elseif ($param_notcurated = 'notverified' && $param_curated == 'notcurated' && $acf_curated == 1 || $acf_curated[0] == 'verified') {
                                $new_posts[] = $post;
                            }
                        }
                    }
                }
            }
        }
        if (count($new_posts) > 0):
            $query->posts = $new_posts;
        else:
            echo '<h3 class="blue light">No opportunities available at this time, please check back later.</h3>';
            $query->posts = array();
        endif;
    }
    
    wp_reset_query();
    return $query;
}
add_filter('wpv_filter_query_post_process', 'jobs_filter', 100, 3);
function jobs_filter($query, $view_settings, $view_id) {
    
    if ($view_id == 325) {
        $param_industry = get_query_var('industry', 'All');
        $param_interest = get_query_var('interests', 'All');
        $param_location = get_query_var('location', 'location');
        $param_company = get_query_var('company', 'company');
        $param_jobtype = get_query_var('jobtype', 'All');
        $param_paid = get_query_var('paid', 'paid');
        $new_posts = array();
        foreach ($query->posts as $post) {
            $acf_industries = get_field('industry', $post->ID);
            $acf_interests = get_field('interests', $post->ID);
            $acf_location = get_field('location_name', $post->ID);
            $acf_company = get_field('company_name', $post->ID);
            $acf_jobtype = get_field('type', $post->ID);
            $acf_paid = get_field('compensated', $post->ID);
            if (!is_array($acf_industries) || !is_array($acf_interests)) {
            } 
            elseif ((is_array($acf_industries) && in_array($param_industry, $acf_industries)) || $param_industry == 'All') {
                if ((is_array($acf_interests) && in_array($param_interest, $acf_interests)) || $param_interest == 'All') {
                    if (strcasecmp($param_location, $acf_location) == 0 || $param_location == 'location' || $param_location == '') {
                        if (strcasecmp($param_company, $acf_company) == 0 || $param_company == 'company' || $param_company == '') {
                            if (strcasecmp($acf_jobtype, $param_jobtype) == 0 || $param_jobtype == 'All') {
                                if ($param_paid == $acf_paid || $param_paid == 'paid') {
                                    $new_posts[] = $post;
                                }
                            }
                        }
                    }
                }
            }
        }
        if (count($new_posts) > 0):
            $query->posts = $new_posts;
        else:
            echo '<h3 class="blue light">No opportunities available at this time, please check back later.</h3>';
            $query->posts = array();
        endif;
    }
    wp_reset_query();
    return $query;
}
