<?php
/**
 * Template Name: Sponsors & Partners
 */
?>
<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page-sponsors-partners'); ?>
<?php endwhile; ?>
