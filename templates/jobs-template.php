<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-offset-1 col-lg-10">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="listing-cards">
                <?php
                function getJsonFeed($url) {
                $json_file = file_get_contents($url);
                $jobs = json_decode($json_file);
                date_default_timezone_set('America/Chicago');
                return $jobs;
                }
                $jobs = getJsonFeed('https://futurefounders.secure.force.com/services/apexrest/jobs/.json');
                $i = 0;
                $count = 0;
                foreach ($jobs as $job) {
                $param_type = get_query_var('type', 'all');
                $param_age = get_query_var('grade_level', 'all');
                $param_session = get_query_var('session', 'all');
                $param_from = get_query_var('from_date', '4');
                $from_date = DateTime::createFromFormat('Y-m-d', $param_from);
                $param_to = get_query_var('to_date', 5);
                $to_date = DateTime::createFromFormat('Y-m-d', $param_to);
                
                $grade_level = preg_replace('/\s+/', ' ', str_replace(' ', '', $job->{'Grade_Level__c'}));
                $group_size = ($job->{'Group_Size__c'});
                //echo $group_size;
                $volunteer_type = strtolower($job->{'Type__c'});
                $volunteer_date_object = new DateTime($job->{'GW_Volunteers__First_Shift__c'});
                $volunteer_date_object->setTimeZone(new DateTimeZone('America/Chicago'));
                $volunteer_date = $volunteer_date_object->format("l, F d");
                $volunteer_start_time = $volunteer_date_object->format("g:ia");
                $volunteers_needed = $job->{'GW_Volunteers__Number_of_Volunteers_Still_Needed__c'};
                $name = $job->{'Name'};
                $id = $job->{'Id'};
                $start_date = $volunteer_date_object->format("n/d/Y");
                $location = $job->{'Location_Name__c'};
                $shift_slots = (array)$job->{'GW_Volunteers__Volunteer_Job_Slots__r'}->{'records'};
                $records = (array)$shift_slots[0]->{'End_Date_Time__c'};
                $end_time_string = $records[0];
                $end_time = new DateTime($end_time_string);
                $end_time->setTimeZone(new DateTimeZone('America/Chicago'));
                $final_end_time = $end_time->format("g:ia");
                $show_on_site = $job->{'GW_Volunteers__Display_on_Website__c'};
                if ($show_on_site == true) {
                if ($param_type == $volunteer_type || $param_type == 'all') {
                if (strcasecmp($param_age,$grade_level) == 0 || $param_age == 'all') {
                if ($param_session == $group_size || $param_session == 'all') {
                if ($from_date < $volunteer_date_object || $from_date == '') {
                if ($to_date > $volunteer_date_object || $to_date == '') {
                $count++;
                if ($i % 3 == 0) {
                ?>
                
                <div class="row">
                    
                    <?php }
                    ?>
                    
                    
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <div class="listing-body">
                            
                            <?php if ($volunteer_type == 'facilitate') { ?>
                            <div id="volunteer-title" class="border-light-blue">
                                <h5 class="light blue">
                                <span class="highlight-light-blue white"><?php echo $volunteer_type; ?></span>
                                &nbsp;&nbsp;<?php echo "#" . $grade_level . "&nbsp;&nbsp;#" . $group_size  ?>
                                </h5>
                            </div>
                            <?php } if ($volunteer_type == 'mentor') { ?>
                            <div id="volunteer-title" class="border-pink">
                                <h5 class="light blue"><span class="highlight-pink white"><?php echo $volunteer_type; ?></span>
                                &nbsp;&nbsp;<?php echo "#" . $grade_level . "&nbsp;&nbsp;#" . $group_size  ?>
                                </h5>
                            </div>
                            <?php } if ($volunteer_type == 'speak') { ?>
                            <div id="volunteer-title" class="border-purple">
                                <h5 class="light blue "><span class="highlight-purple white"><?php echo $volunteer_type; ?></span>
                                &nbsp;&nbsp;<?php echo "#" . $grade_level . "&nbsp;&nbsp;#" . $group_size  ?>
                                </h5>
                            </div>
                            <?php } ?>
                            <div class="row">
                                <div style="white-space:nowrap;" class="col-xs-7 col-sm-7 col-md-7 col-lg-7" id="date-text">
                                    <h5 class="blue"><?php echo $volunteer_date ?></h5>
                                </div>
                                <div style="white-space:nowrap;" class="col-xs-5 col-sm-5 col-md-5 col-lg-5" id="remaining-text">
                                    <?php if ($volunteers_needed > 1) { ?>
                                    <h5 class="blue light"><b><?php echo $volunteers_needed ?></b> <span style="font-weight: 300">SPOTS REMAIN</span></h5>
                                    <?php } ?>
                                    <?php if ($volunteers_needed == 1) { ?>
                                    <h5 class="blue light"><b><?php echo $volunteers_needed ?></b> <span style="font-weight: 300">SPOT REMAINS</span></h5>
                                    <?php } ?>
                                    <?php if ($volunteers_needed == 0) { ?>
                                    <h5 class="blue light">0 <span style="font-weight: 300">SPOTS REMAIN</span></h5>
                                    <?php } ?>
                                </div>
                            </div>
                            <div id="time-text">
                                <h4 class="blue light"><?php echo $volunteer_start_time ?> - <?php echo $final_end_time ?></h4>
                            </div>
                            <h3 class="blue light name-text"><?php echo $name?></h3>
                            <div class="row">
                                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" id="location-text">
                                    <h5 style="white-space: normal!important;" class="blue light"><?php echo $location ?></h5>
                                </div>
                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 text-right" id="register-text">
                                    <?php if ($volunteer_type == 'facilitate') { ?>
                                    <a class="light-blue" href="
                                        <?php
                                        $arr_params = array('volunteer_job' => $id);
                                        echo esc_url(add_query_arg($arr_params, get_permalink('5525')));
                                        ?>">
                                        register now >
                                    </a>
                                    <?php } if ($volunteer_type == 'mentor') { ?>
                                    <a class="pink" href="
                                        <?php
                                        $arr_params = array('volunteer_name' => $name, 'volunteer_date' => $volunteer_date, 'volunteer_start_time' => $volunteer_start_time, 'location' => $location, 'volunteer_job' => $id);
                                        echo esc_url(add_query_arg($arr_params, get_permalink('5525')));
                                        ?>">
                                        register now >
                                    </a>
                                    <?php } if ($volunteer_type == 'speak') { ?>
                                    <a class="purple" href="
                                        <?php
                                        $arr_params = array('volunteer_name' => $name, 'volunteer_date' => $volunteer_date, 'volunteer_start_time' => $volunteer_start_time, 'location' => $location, 'volunteer_job' => $id);
                                        echo esc_url(add_query_arg($arr_params, get_permalink('5525')));
                                        ?>">
                                        register now >
                                    </a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $i++;
                    if ($i % 3 == 0)
                    {
                    ?>
                </div>
                <div class="clearfix"></div>
                
                <?php
                }
                }
                } } } } }

                } 

                if ($count == 0) { ?>
                    <h3 class="light blue">No volunteer opportunities found, please refine your search.</h3>
                <?php } ?>
            </div>
        </div>
        <div class="col-lg-1"></div>
    </div>
</div>