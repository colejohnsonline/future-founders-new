<?php while (have_posts()) : the_post(); ?>
<div style="margin-top: 75px;" id="detail-content" class="container-fluid">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div id="volunteer-job-detail">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <article <?php post_class(); ?>>
              <header class="hidden-xs">
                <h1 style="white-space:pre-wrap; display: inline;" class="white"><span class="highlight-navy"><?php the_title(); ?></span></h1>
              </header>
              <header class="hidden-sm hidden-md hidden-lg">
                <h1 style="white-space:pre-wrap; display: inline;" class="blue"><span><?php the_title(); ?></span></h1>
              </header>
            </article>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="tags">
              <?php 
              $industries = get_field("industry");
              foreach ($industries as $i) {
              ?>
              <a style="font-size: 14px;" target="_blank" href="<?php echo site_url(); ?>/startup/news/?industry=<?php echo $i; ?>"><?php echo '#' . $i ?></a>
              <?php } ?>
              <?php 
              $industries = get_field("interests");
              foreach ($industries as $i) {
              ?>
              <a style="font-size: 14px;" target="_blank" href="<?php echo site_url(); ?>/startup/news/?interest=<?php echo $i; ?>"><?php echo '#' . $i ?></a>
              <?php } ?>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="hidden-xs hidden-sm hidden-md col-md-1"></div>
          <div class="col-md-2 col-lg-2">
            <div id="stamp">
              <?php if (!get_field('future_founders_news')) { ?>
              <?php $curated = get_field('future_founders_verified');
              if ($curated == true) { ?>
                <h5 class="white">CURATED BY<br>FUTURE FOUNDERS</h5>
              <?php } ?>
              <?php } ?>
            </div>
            <div id="author">
              <h5 class="author blue"><a target="_blank" href="<?php echo get_field('press_hyperlink'); ?>"><?php echo get_field('news_source'); ?></a></h5>
            </div>
            <div id="social" style="margin-top: 17px;">
              <h4 style="margin-bottom: 15px;" class="blue">Spread The Word!</h4>
              <?php echo do_shortcode( '[feather_share show="facebook,linkedin" hide="reddit, pinterest, twitter, google_plus, tumblr, mail"]' ); ?>
              <?php $arr_params = array('volunteer_job' => $id);
              ?>
              <a href="http://twitter.com/share?url=<?php echo get_permalink(); ?>;text=<?php echo the_title(); ?>;size=small&amp;count=none" target="_blank">
                <div>
                  <img style="width:35px; height:35px;" src="<?= get_template_directory_uri(); ?>/dist/images/twitter_social.png"/>
                </div>
              </a>
            </div>
          </div>
          <div class="col-md-4 col-lg-4">
            <div id="content">
              <h4 class="blue"><?php echo get_field('left_column') ?></h4>
              <?php if (get_field('youtube_code')) { ?>
              <iframe type="text/html" width="480" height="320"
              src="http://www.youtube.com/embed/<?php echo get_field('youtube_code'); ?>"
              frameborder="0"/></iframe>
              <?php } ?>
              <?php if (get_field('press_hyperlink')) { ?>
              <div style="margin-top:40px;">
                <a target="_blank" class="btn-primary" href="<?php echo get_field('press_hyperlink'); ?>">Read More</a></p>
              </div>
              <?php } ?>
            </div>
          </div>
          <div class="col-md-4 col-lg-4">
            <div id="content" class="job-details">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="img-responsive"><?php the_post_thumbnail('medium', array('class' => 'alignleft')); ?></div>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <?php if (get_field('press_document')) { ?>
                  <p class="blue"><a href="<?php echo get_field('press_document'); ?>"><?php echo get_field('press_document'); ?></a></p>
                  <?php } ?>
                  <h4 class="blue"><?php echo get_field('right_column') ?></h4>
                </div>
              </div>
            </div>
          </div>
          <div class="hidden-xs hidden-sm hidden-md col-md-1"></div>
        </div>
        <div class="bullet-list">
          <div class="row">
            <div class="content-blue">
              <div class="col-md-2">
              </div>
              <div class="col-md-5">
                <p class="blue"><?php echo get_field('left_bullet_list') ?></p>
              </div>
              <div class="col-md-5">
                <p class="blue"><?php echo get_field('right_bullet_list') ?></p>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-2 col-lg-2"></div>
          <div style="margin-top:60px;" class="col-md-10 col-lg-10">
            <h4 class="blue"><a href="<?php echo site_url(); ?>/startup/news">< BACK TO NEWS</a></h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <style type="text/css">
  .sumome-share-client-wrapper.sumome-share-client-wrapper-left-page {
  top: 340px!important;
  }
  p {
  line-height: 1.4;
  font-size:;
  }
  span {
  line-height: 1.4;
  }
  #content li {
  font-weight: 300;
  line-height: 1.4;
  font-size: 16px;
  }
  .job-details h3 {
  margin-bottom: -12px;
  }
  p a {
  text-decoration: underline !important;
  }
  a:hover {
  color: #00bce4;
  }
  #detail-content img {
  width: 100%;
  height: 100%;
  }
  </style>
  <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
</footer>
<?php comments_template('/templates/comments.php'); ?>
</article>
</div>
<?php endwhile; ?>
<script>window.twttr = (function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0],
t = window.twttr || {};
if (d.getElementById(id)) return t;
js = d.createElement(s);
js.id = id;
js.src = "https://platform.twitter.com/widgets.js";
fjs.parentNode.insertBefore(js, fjs);
t._e = [];
t.ready = function(f) {
t._e.push(f);
};
return t;
}(document, "script", "twitter-wjs"));</script>