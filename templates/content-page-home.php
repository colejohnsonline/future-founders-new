<div id="home" class="front-page">
	<div class="hidden-xs">
		<video style="margin-top: 80px;" autoplay loop preload="auto" muted id="bgvid">
			<source src="<?php bloginfo('template_url'); ?>/video/moon-landing.mp4" type="video/mp4">
			<source src="<?php bloginfo('template_url'); ?>/video/moon-landing.webm" type="video/webm">
		</video>
	</div>
	<div class="hidden-sm hidden-md hidden-lg">
		<img class="front-page-banner" src="<?= get_template_directory_uri(); ?>/dist/images/homepage_fallback.jpg"/>
	</div>
	
	<section class="banner">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="content">
						<div class="home-heading">
							<h1><span class="highlight-opacity"><?php echo get_field('banner_title'); ?></span></h1>
						</div>
						<div class="home-body">
							<p class="white">
								<span class="highlight-opacity"><?php echo get_field('banner_description'); ?></span>
							</p>
						</div>
						<div class="home-buttons">
							<div class="row">
								<div class="hidden-xs hidden-sm col-md-6">
									<form action="<?php echo get_permalink(5407) ?>">
										<input style="max-width: 290px;" class="btn-primary pull-right" type="submit" value="I Want to Volunteer">
									</form>
								</div>
								<div class="hidden-xs hidden-sm col-md-6">
									<form action="<?php echo get_permalink(5511) ?>">
										<input style="max-width: 290px; padding-left: 47px !important;" class="btn-green pull-left" type="submit" value="I Want the Startup Program">
									</form>
								</div>
								<div class="col-xs-12 col-sm-12 hidden-md hidden-lg text-center" style="margin-top:20px;margin-bottom:20px;">
									<form action="<?php echo get_permalink(5407) ?>">
										<input style="max-width: 290px;" class="btn-primary" type="submit" value="I Want to Volunteer">
									</form>
								</div>
								<div class="col-xs-12 col-sm-12 hidden-md hidden-lg text-center">
									<form action="<?php echo get_permalink(5511) ?>">
										<input style="max-width: 290px; padding-left: 47px !important;" class="btn-green" type="submit" value="I Want the Startup Program">
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="sub-banner">
		<a href="<?php the_permalink(); ?>programs">
			<div class="container">
				<div class="row">
				</div>
			</div>
		</a>
	</section>
	<section class="featured-news">
		<div class="container-fluid">
			<div class="row">
				<?php
					$featured_post = get_field('featured_news');
					$post_id = $featured_post->ID;
					$date = get_the_date('l, F j', $post_id); // apply_filters( 'the_content', $featured_post->post_content );
					$post_author = apply_filters( 'the_content', $featured_post->post_author);
					$author = the_author_meta( 'user_nicename', $post_author );
					$title = apply_filters( 'the_content', $featured_post->post_title );
				?>
				<div class="col-sm-12 col-md-offset-1 col-md-5 col-lg-offset-1 col-lg-5">
					<div id="featured-img">
						<!-- <img class="img-responsive" src="<?= get_template_directory_uri(); ?>/dist/images/featured_news.jpg"> -->
						<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post_id)); ?>
						<img class="img-responsive" src="<?php echo $feat_image ?>">
					</div>
				</div>
				<div class="col-sm-12 col-md-5 col-lg-5">
					<div id="news-post">
						<div class="links-header">
							<a href="<?php echo get_permalink($post_id); ?>"><h3 style="line-height: 40px;" class="serif"><?php echo $featured_post->post_title; ?> </h3></a>
						</div>
						<p class="other-blue"><?php echo $author ?></p>
						<?php echo get_field('featured_news_description') ?>
						<a class="other-blue" href="<?php echo get_permalink($post_id); ?>">Read More ></a>
					</div>
				</div>
				<div class="col-md-1 col-lg-1"></div>
			</div>
		</div>
		<div class="clearfix visible-xs-block"></div>
		
		<section class="content-links">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">
						<div id="news" class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
							<div class="links-header">
								<h3 class="serif">News</h3>
							</div>
							<?php
							$args = array( 'numberposts' => 3, 'post_type' => 'news-article',
								'meta_query' => array(
									array(
										'key' => 'show_home_page',
										'value' => '1',
										'compare' => '='
										)
									)
								);
							$myposts = get_posts( $args );
							foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
							<?php $latest_post_id = $post->ID; ?>
							<?php if (wp_get_attachment_url( get_post_thumbnail_id($latest_post_id))) {
							$latest_image = wp_get_attachment_url( get_post_thumbnail_id($latest_post_id));
								} else {
							$latest_image = get_template_directory_uri() . "/dist/images/latest-news.png";
							}
								$fallback_thumbnail = get_template_directory_uri() . "/dist/images/latest-news.png";
							?>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 news-row">
								<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
									<div class="featured-img">
										<?php if (has_post_thumbnail()) { ?>
										<?php the_post_thumbnail('small'); ?>
										<?php } else { ?>
										<img style="max-width:150px;max-height:150px;" src="<?php echo $fallback_thumbnail; ?>">
										<?php } ?>
									</div>
								</div>
								<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
									<div class="news-header">
										<a style="font-weight: 300; display: inline-block;" href="<?php the_permalink(); ?>"><?php echo $post->post_title; ?></a>
									</div>
								</div>
								<div class="clearfix"></div>
								</br>
							</div>
							<?php endforeach; ?>
							<?php
							wp_reset_postdata();
							?>
						</div>
						
						<div id="startup" class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
							<div class="links-header">
								<h3 class="serif">Startup</h3>
							</div>
							<div class="links">
								<h2>News</h2>
								<h4><a class="green" href="<?php the_permalink(); ?>startup/news">SEE MORE STARTUP NEWS ></a></h4>
								<a href="<?php the_permalink(); ?>startup/news"><p class="border-navy">learn about startup news</p></a>
								<br/>
								<h2>Events</h2>
								<h4><a class="other-blue" href="<?php the_permalink(); ?>startup/events">CHECK OUT MORE STARTUP EVENTS ></a></h4>
								<a href="<?php the_permalink(); ?>startup/events"><p class="border-navy">learn about startup events</p></a>
								<br/>
								<h2>Jobs</h2>
								<h4><a class="orange" href="<?php the_permalink(); ?>startup/jobs">BROWSE STARTUP JOBS ></a></h4>
								<p class="border-navy"><a href="<?php the_permalink(); ?>startup/jobs">learn about startup jobs</a></p>
							</div>
						</div>
						<div id="ignite" class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
							<div style="white-space:nowrap;"class="links-header">
								<h3 class="serif">Discover &amp; Ignite</h3>
							</div>
							<div class="links" style="white-space:nowrap;">
								<h2>Facilitate</h2>
								<h4><a class="light-blue" href="<?php the_permalink(); ?>get-involved/facilitate">FACILITATE WITH FUTURE FOUNDERS ></a></h4>
								<p class="border-navy"><a href="<?php the_permalink(); ?>get-involved/facilitate">learn about facilitating with us</a></p>
								<br/>
								<h2>Mentor</h2>
								<h4><a class="pink" href="<?php the_permalink(); ?>get-involved/mentoring">MENTOR WITH FUTURE FOUNDERS ></a></h4>
								<p class="border-navy"><a href="<?php the_permalink(); ?>get-involved/mentoring">learn about mentorship</a></p>
								<br/>
								<h2>Speak</h2>
								<h4><a class="purple" href="<?php the_permalink(); ?>get-involved/speak">SPEAK WITH FUTURE FOUNDERS ></a></h4>
								<p class="border-navy"><a href="<?php the_permalink(); ?>get-involved/speak">learn about speaking with us</a></p>
							</div>
						</div>
					</div>
					<div class="col-md-1 col-lg-1"></div>
				</div>
			</div>
		</section>
		
		<section class="bottom-banners">
			<div class="container-fluid nopadding">
				<a href="<?php the_permalink(); ?>programs/discover">
					<section id="discover" class="info-banner-discover">
						<div class="row">
							<div class="col-md-12">
								<div class="home-heading">
									<h3 class="light serif">Future Founders</h3>
									<h1><span class="highlight-opacity">DISCOVER</span></h1>
									<p class="light white"><img width="10px" src="<?= get_template_directory_uri(); ?>/dist/images/info-icon.png">&nbsp;&nbsp;&nbsp;learn about discover</p>
								</div>
							</div>
						</div>
					</section>
				</a>
				<a href="<?php the_permalink(); ?>programs/ignite">
					<section id="ignite" class="info-banner-ignite">
						<div class="row">
							<div class="col-md-12">
								<div class="home-heading">
									<h3 class="light blue serif">Future Founders</h3>
									<h1><span class="highlight-opacity">IGNITE</span></h1>
									<p class="light blue "><img width="10px" src="<?= get_template_directory_uri(); ?>/dist/images/info-icon.png">&nbsp;&nbsp;&nbsp;learn about ignite</p>
								</div>
							</div>
						</div>
					</section>
				</a>
				
				<a href="<?php the_permalink(); ?>programs/startup">
					<section id="startup" class="info-banner-startup">
						<div class="row">
							<div class="col-md-12">
								<div class="home-heading">
									<h3 class="light blue serif">Future Founders</h3>
									<h1><span class="highlight-opacity">STARTUP</span></h1>
									<p class="light blue"><img width="10px" src="<?= get_template_directory_uri(); ?>/dist/images/info-icon.png">&nbsp;&nbsp;&nbsp;learn about startup</p>
								</div>
							</div>
						</div>
					</section>
				</a>
			</div>
		</section>
	</div>
	<style type="text/css">
	#sponsors {
		margin-top: 80px;
	}
	.featured-img img {
		width: 100%;
		height: 100%;
		max-width: 110px;
		max-height: 110px;
	}
	</style>