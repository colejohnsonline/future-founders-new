<?php
function getJsonFeed($url) {
    $json_file = file_get_contents($url);
    $jobs = json_decode($json_file);
    date_default_timezone_set('America/Chicago');
    return $jobs;
}
$volunteer_job = get_query_var('volunteer_job', 1);
$i = 0;
$jobs = getJsonFeed('https://futurefounders.secure.force.com/services/apexrest/jobs/.json');
foreach ($jobs as $job) {
    $id = $job->{'Id'};
    if ($id == $volunteer_job) {
        $volunteer_type = strtolower($job->{'Type__c'});
        $volunteer_date_object = new DateTime($job->{'GW_Volunteers__First_Shift__c'});
        $volunteer_date_object->setTimeZone(new DateTimeZone('America/Chicago'));
        $volunteer_date = $volunteer_date_object->format("l, F d, Y");
        $volunteer_start_time = $volunteer_date_object->format("g:ia");
        $volunteers_needed = $job->{'GW_Volunteers__Number_of_Volunteers_Still_Needed__c'};
        $name = $job->{'Name'};
        $start_date = $volunteer_date_object->format("n/d/Y");
        $location = $job->{'Location_Name__c'};
        $description = $job->{'GW_Volunteers__Description__c'};
        $session_type = $job->{'Group_Size__c'};
        $grade = $job->{'Grade_Level__c'};
        $street = $job->{'GW_Volunteers__Location_Street__c'};
        $city = $job->{'GW_Volunteers__Location_City__c'};
        $state = $job->{'GW_Volunteers__Location__c'};
        $zip = $job->{'GW_Volunteers__Location_Zip_Postal_Code__c'};
        $shift_slots = (array)$job->{'GW_Volunteers__Volunteer_Job_Slots__r'}->{'records'};
        $records = (array)$shift_slots[0]->{'End_Date_Time__c'};
        $end_time_string = $records[0];
        $end_time = new DateTime($end_time_string);
        $end_time->setTimeZone(new DateTimeZone('America/Chicago'));
        $final_end_time = $end_time->format("g:ia");
        $job_id = $id;
        $shift = (array)$shift_slots[0]->{'Id'};
        $shift_id = $shift[0];
        $group_size = ($job->{'Group_Size__c'});
        
        // $shift_id_hey = $job -> {'GW_Volunteers__Volunteer_Job_Slots__r.records.Name'};
        
    }
} ?>
<div style="margin-top: 75px;" id="detail-content" class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="volunteer-job-detail">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <article>
                            <header class="hidden-xs">
                                <h1 style="white-space:pre-wrap; display: inline;" class="white"><span class="highlight-navy"><?php
                                echo $name ?></span></h1>
                            </header>
                            <header class="hidden-sm hidden-md hidden-lg">
                                <h1 style="white-space:pre-wrap; display: inline;" class="blue"><span><?php
                                echo $name ?></span></h1>
                            </header>
                        </article>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="margin-top: 30px;">
                        <p class="blue" style="font-size: 8px; font-weight: 600;"><?php
                        echo "#" . $volunteer_type ?>&nbsp;&nbsp;&nbsp;<?php
                        echo "#" . $grade ?>&nbsp;&nbsp;&nbsp;<?php
                        echo "#" . $group_size ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="hidden-xs hidden-sm col-md-2 col-lg-2">
                        <div id="social" style="margin-top: 17px;">
                            <h4 style="margin-bottom: 15px;" class="blue">Spread The Word!</h4>
                            <?php
                            $arr_params = array('volunteer_job' => $id);
                            ?>
                            <div style="width: 35px; height: 35px;" >
                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php
                                echo esc_url(add_query_arg($arr_params, get_permalink('5525'))); ?>"
                                onclick="window.open(this.href,'targetWindow',
                                   'toolbar=no,
                                    location=no,
                                    status=no,
                                    menubar=no,
                                    scrollbars=yes,
                                    resizable=yes,
                                    width=400,
                                    height=400');
                                return false;">
                                <img width="35" height="35" src="<?php echo get_template_directory_uri(); ?>/dist/images/facebook_social.png"/>
                                </a>
                            </div>
                            <div style="width: 35px; height: 35px;" >
                                <a href="http://twitter.com/share?url=<?php
                                echo esc_url(add_query_arg($arr_params, get_permalink('5525'))); ?>;text=<?php
                                echo $name; ?>;size=small&amp;count=none"
                                onclick="window.open(this.href,'targetWindow',
                                'toolbar=no,
                                location=no,
                                status=no,
                                menubar=no,
                                scrollbars=yes,
                                resizable=yes,
                                width=400,
                                height=400');
                                return false;">
                                <img width="35" height="35" src="<?php echo get_template_directory_uri(); ?>/dist/images/twitter_social.png"/>
                                </a>
                            </div>
                            <div style="width: 35px; height: 35px;" >
                                <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php
                                echo esc_url(add_query_arg($arr_params, get_permalink('5525'))); ?>&title=<?php
                                echo $name; ?>" target="_blank">
                                <img width="35" height="35" src="<?php echo get_template_directory_uri(); ?>/dist/images/linkedin_social.png"/>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div id="content">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <p class="light blue"><?php
                                    echo $description; ?></p>
                                    <?php
                                    if ($volunteers_needed > 0) { ?>
                                    <h3>Sign-Up Today</h3>
                                    <?php
                                    //echo $job_id;
                                    ?>
                                    <?php
                                    //echo $shift_id;
                                    ?>
                                    <?php
                                    if (isset($_POST['submit'])) {
                                    $_url = 'https://futurefounders.secure.force.com/services/apexrest/signup';
                                    unset($_POST['submit']);
                                    $data_string = json_encode($_POST);
                                    $ch = curl_init($_url);
                                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data_string)));
                                    $result = curl_exec($ch);

                                    //  echo 'JSON DATA<br>';
                                    //   echo $data_string;
                                    echo '<hr>';
                                    $result_ary = json_decode($result);
                                    echo $result_ary->volunteerHourMsg;

                                    //     var_dump($result);

                                    }
                                    ?>
                                    <form action="" method="post" enctype="application/json">
                                        <div class="form-group">
                                            <label for="firstname">First Name</label>
                                            <input type="text" name="parameter[contact][firstname]" class="form-control" id="firstname" placeholder="" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">Last Name</label>
                                            <input type="text" name="parameter[contact][lastname]" class="form-control" id="lastname" placeholder="" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="company">Company</label>
                                            <input type="text" name="parameter[contact][company__c]" class="form-control" id="company" placeholder="" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email Address</label>
                                            <input type="email" name="parameter[contact][email]" class="form-control" id="email" placeholder="" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="workphone">Phone</label>
                                            <input type="text" name="parameter[contact][npe01__workphone__c]" class="form-control" id="workphone" placeholder="" required>
                                        </div>
                                        <input type="hidden" name="parameter[hour][GW_Volunteers__Number_of_Volunteers__c]" value="1">
                                        <input type="hidden" name="parameter[shiftIdSignUp]" value="<?php
                                        echo $shift_id; ?>">
                                        <input type="hidden" name="parameter[jobIdSignUp]" value="<?php
                                        echo $job_id; ?>">
                                        <input type="submit" class="btn btn-default" name="submit" value="submit">
                                    </form>
                                    <?php
                                    } ?>
                                    <?php
                                    if ($volunteers_needed == 0) { ?>
                                    <h3>You can no longer register for this opportunity</h3>
                                    <?php
                                    } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div id="content" class="job-details">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- INSERT MAP -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="location-map img-responsive">
                                        <iframe
                                        width="450"
                                        height="400"
                                        frameborder="0" style="border:0"
                                        src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDiVEy8GEh61VW5RG-oZ2xoMb1FyT51f-U&q=<?php
                                        echo "$street, $city, $state, $zip" ?>" allowfullscreen>
                                        </iframe>
                                    </div>
                                    <h2 class="serif blue">Volunteer Details</h2>
                                    <div class="item">
                                        <h3 class="light blue">WHERE:</h3>
                                        <h3 class="blue"><?php
                                        echo "$street, $city, $state $zip" ?></h3>
                                    </div>
                                    <div class="item">
                                        <h3 class="light blue">WHEN:</h3>
                                        <h3 class="blue"><?php
                                        echo $volunteer_date ?> <?php
                                        echo "$volunteer_start_time" ?> - <?php
                                        echo "$final_end_time" ?></h3>
                                    </div>
                                    <div class="item">
                                        <h3 class="light blue">NUMBER OF SPOTS REMAINING:</h3>
                                        <h3 class="blue"><?php
                                        echo $volunteers_needed ?></h3>
                                    </div>
                                    <div class="item">
                                        <h3 class="light blue">OPPORTUNITY TYPE:</h3>
                                        <h3 class="blue"><?php
                                        echo $volunteer_type ?></h3>
                                    </div>
                                    <div class="item">
                                        <h3 class="light blue">SESSION TYPE:</h3>
                                        <h3 class="blue"><?php
                                        echo $session_type ?></h3>
                                    </div>
                                    <div class="item">
                                        <h3 class="light blue">SCHOOL:</h3>
                                        <h3 class="blue"><?php
                                        echo $location ?></h3>
                                    </div>
                                    <div class="item">
                                        <h3 class="light blue">GRADE:</h3>
                                        <h3 class="blue"><?php
                                        echo $grade ?></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="hidden-xs hidden-sm col-md-2 col-lg-2"></div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-lg-2"></div>
                    <div style="margin-top:60px;" class="col-md-10 col-lg-10">
                        <h4 class="blue"><a href="<?php
                        echo site_url(); ?>/get-involved/opportunities">< BACK TO VOLUNTEER EVENTS</a></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" style="margin-left: -30px!important; margin-right: -30px;!important">
    <div class="hidden-sm hidden-xs detail-carousel">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php
            echo do_shortcode('[wpv-view name="FF News Slider"]'); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-offset-1 col-lg-10">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="volunteer-heading">
                <h1><span class="highlight-navy">Featured Opportunities</span></h1>
            </div>
            <div class="listing-cards">
                <?php
                $i = 0;
                foreach ($jobs as $job) {
                if ($i % 3 == 0 && $i != 0) { ?>
            </div>
            <div class="clearfix"></div>
            <?php
            break;
            ?>
            <div class="row">
                <?php
                }
                $volunteer_type = strtolower($job->{'Type__c'});
                $volunteer_date_object = new DateTime($job->{'GW_Volunteers__First_Shift__c'});
                $volunteer_date_object->setTimeZone(new DateTimeZone('America/Chicago'));
                $volunteer_date = $volunteer_date_object->format("l, F d");
                $volunteer_start_time = $volunteer_date_object->format("g:ia");
                $volunteers_needed = $job->{'GW_Volunteers__Number_of_Volunteers_Still_Needed__c'};
                $name = $job->{'Name'};
                $id = $job->{'Id'};
                $start_date = $volunteer_date_object->format("n/d/Y");
                $location = $job->{'Location_Name__c'};
                $shift_slots = (array)$job->{'GW_Volunteers__Volunteer_Job_Slots__r'}->{'records'};
                $records = (array)$shift_slots[0]->{'End_Date_Time__c'};
                $end_time_string = $records[0];
                $end_time = new DateTime($end_time_string);
                $end_time->setTimeZone(new DateTimeZone('America/Chicago'));
                $final_end_time = $end_time->format("g:ia");
                ?>
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="listing-body">
                        <?php
                        if ($volunteer_type == 'mentor') { ?>
                        <div id="volunteer-title" class="border-light-blue">
                            <h5 class="light"><span class="highlight-light-blue"><?php
                            echo $volunteer_type; ?></span></h5>
                        </div>
                        <?php
                        }
                        if ($volunteer_type == 'speak') { ?>
                        <div id="volunteer-title" class="border-pink">
                            <h5 class="light"><span class="highlight-pink"><?php
                            echo $volunteer_type; ?></span></h5>
                        </div>
                        <?php
                        }
                        if ($volunteer_type == 'facilitate') { ?>
                        <div id="volunteer-title" class="border-purple">
                            <h5 class="light"><span class="highlight-purple"><?php
                            echo $volunteer_type; ?></span></h5>
                        </div>
                        <?php
                        } ?>
                        <div class="row">
                            <div  style="white-space:nowrap;" class="col-xs-7 col-sm-7 col-md-7 col-lg-7" id="date-text">
                                <h5 class="blue"><?php
                                echo $volunteer_date ?></h5>
                            </div>
                            <div style="white-space:nowrap;" class="col-xs-5 col-sm-5 col-md-5 col-lg-5" id="remaining-text">
                                <?php
                                if ($volunteers_needed > 1) { ?>
                                <h5 class="blue light"><b><?php
                                echo $volunteers_needed ?></b> <span style="font-weight: 300">SPOTS REMAIN</span></h5>
                                <?php
                                } ?>
                                <?php
                                if ($volunteers_needed == 1) { ?>
                                <h5 class="blue light"><b><?php
                                echo $volunteers_needed ?></b> <span style="font-weight: 300">SPOT REMAINS</span></h5>
                                <?php
                                } ?>
                                <?php
                                if ($volunteers_needed == 0) { ?>
                                <h5 class="blue light">0 <span style="font-weight: 300">SPOTS REMAIN</span></h5>
                                <?php
                                } ?>
                            </div>
                        </div>
                        <div id="time-text">
                            <h4 class="blue light"><?php
                            echo $volunteer_start_time ?> - <?php
                            echo $final_end_time ?></h4>
                        </div>
                        <h3 class="blue light name-text"><?php
                        echo $name ?></h3>
                        <div class="row">
                            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" id="location-text">
                                <h5 class="blue light"><?php
                                echo $location ?></h5>
                            </div>
                            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 text-right" id="register-text">
                                <?php
                                if ($volunteer_type == 'mentor') { ?>
                                <a class="light-blue" href="
                                <?php
                                $arr_params = array('volunteer_name' => $name, 'volunteer_date' => $volunteer_date, 'volunteer_start_time' => $volunteer_start_time, 'location' => $location, 'volunteer_job' => $id);
                                echo esc_url(add_query_arg($arr_params, get_permalink('5525')));
                                ?>">
                                register now >
                                </a>
                                <?php
                                }
                                if ($volunteer_type == 'speak') { ?>
                                <a class="pink" href="
                                <?php
                                $arr_params = array('volunteer_name' => $name, 'volunteer_date' => $volunteer_date, 'volunteer_start_time' => $volunteer_start_time, 'location' => $location, 'volunteer_job' => $id);
                                echo esc_url(add_query_arg($arr_params, get_permalink('5525')));
                                ?>">
                                register now >
                                </a>
                                <?php
                                }
                                if ($volunteer_type == 'facilitate') { ?>
                                <a class="purple" href="
                                <?php
                                $arr_params = array('volunteer_name' => $name, 'volunteer_date' => $volunteer_date, 'volunteer_start_time' => $volunteer_start_time, 'location' => $location, 'volunteer_job' => $id);
                                echo esc_url(add_query_arg($arr_params, get_permalink('5525')));
                                ?>">
                                register now >
                                </a>
                                <?php
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                $i++;
                if ($i % 3 == 0) {
                ?>
            </div>
            <div class="clearfix"></div>
            <?php
            }
            } ?>
        </div>
    </div>
    <div class="col-lg-1"></div>
</div>
<style type="text/css">
label {
font-size: 16px !important;
}
input {
border-radius: 0 !important;
}
</style>