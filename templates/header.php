<?php if (!is_page( 'User' )) { ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<?php } ?>
<?php use Roots\Sage\Nav\NavWalker; ?>
<header class="banner navbar navbar-default navbar-fixed-top" role="banner">
  <div class="container-fluid" style="padding: 0;">
    <div class="navbar-header">
      <div class="col-md-12">
        
        <?php if ( is_user_logged_in() ) { ?>
        <div id="nav-profile" style="position:absolute;top:9px;right:110px;">
          <a href="<?php site_url(); ?>/user"><span class="white glyphicon glyphicon-user" aria-hidden="true"></span></a>
        </div>
        <?php } ?>
        
        <div id="nav-search">
          <img class="img-responsive" src="<?= get_template_directory_uri(); ?>/dist/images/nav-search.png">
        </div>
        <button type="button" id="menu-button" class="navbar-toggle collapsed" data-target="">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>"></a>
      </div>
      <div id="full-search" style="overflow: scroll; display: none;">
        <div class="col-md-12">
          <div id="logo-menu">
            <img src="<?= get_template_directory_uri(); ?>/dist/images/FFF-Logo.png">
          </div>
          <button type="button" class="navbar-toggle collapsed" id="nav-search-close">
          <img class="img-responsive" src="<?= get_template_directory_uri(); ?>/dist/images/close-icon.png">
          </button>
          <div id="search-form">
            <form role="search" method="get" class="search-form form-inline" action="<?= esc_url(home_url('/')); ?>">
              <label class="sr-only"><?php _e('Search for:', 'sage'); ?></label>
              <div class="input-group">
                <input type="search" value="<?= get_search_query(); ?>" name="s" class="search-field form-control" placeholder="<?php _e('Search', 'sage'); ?> <?php bloginfo('name'); ?>" required>
                <button type="submit" class="search-submit btn btn-primary"><?php _e('Go!', 'sage'); ?></button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div id="full-menu" style="overflow: scroll; display: none;">
        <div class="row">
          <div class="col-md-12">
            <div id="logo-menu">
              <img src="<?= get_template_directory_uri(); ?>/dist/images/FFF-Logo.png">
            </div>
            <button type="button" class="navbar-toggle collapsed" id="close-button">
            <img class="img-responsive" src="<?= get_template_directory_uri(); ?>/dist/images/close-icon.png">
            </button>
            
            <div class="primary-nav">
              <?php
              if (has_nav_menu('primary_navigation')) :
              wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new NavWalker(), 'menu_class' => 'full-menu']);
              endif;
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
<script type="text/javascript">
$( document ).ready(function() {
$('#menu-button').click(function() {
$('#full-menu').slideToggle("400");
$('html, body').css('position','fixed');
/* $(window).scroll(function() {
$('#full-menu').fadeOut("slow");
});*/
});
$('#close-button').click(function() {
$('#full-menu').slideToggle("400");
$('html, body').css('position','relative');
/*$(window).scroll(function() {
$('#full-menu').fadeOut("slow");
});*/
});
$('#nav-search').click(function() {
$('#full-search').slideToggle("400");
$('html, body').css('position','fixed');
/*
$(window).scroll(function() {
$('#full-search').fadeOut("slow");
});*/
});
$('#nav-search-close').click(function() {
$('#full-search').slideToggle("400");
$('html, body').css('position','relative');
/*$(window).scroll(function() {
$('#full-search').fadeOut("slow");*/
});
});
</script>
<style type="text/css">
@media (min-width: 768px) {
ul#menu-primary-navigation li.dropdown:hover > ul.dropdown-menu {
display: block;
}
}
@media (max-width: 768px) {
ul#menu-primary-navigation ul.dropdown-menu {
display: block;
}
}</style>
<?php if (is_page('Donate')) { ?>
<style type="text/css">
body #volunteer-job-detail .gform_wrapper .gform_body .gform_fields .gfield input[type=text], body .gform_body input[type=text] {
width: 150px;
}
</style>
<?php } ?>