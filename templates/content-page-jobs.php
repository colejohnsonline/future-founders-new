<div class="jobs-listing" id="listing">
	<section class="banner">
		<div class="container">
			<div class="listing-header">
				<div class="home-heading">
					<h1><span class="highlight-opacity">JOBS</span></h1>
				</div>
				<div class="home-body">
					<p class="white">
						<span class="highlight-opacity">find your dream job</span>
					</p>
				</div>
			</div>
		</div>
	</section>
	<div class="row">
		<div id="volunteer-filters">
			<form style="margin-top: 20px" role="form">
				<div class="form-group">
					<select name="industry" class="form-control" id="industry">
						<option value="industry" disabled selected>Industry</option>
						<option value="All">All</option>
						<option value="General">General</option>
						<option value="Tech">Tech</option>
						<option value="Arts">Arts</option>
						<option value="Social Entrepreneurship">Social Entrepreneurship</option>
						<option value="Non-Profit">Non-Profit</option>
					</select>
				</div>
				<div class="form-group">
					<select name="interests" class="form-control" id="interests">
						<option value="interests" disabled selected>Interests</option>
						<option value="All">All</option>
						<option value="Networking">Networking</option>
						<option value="Leadership / Self-Awareness">Leadership / Self-Awareness</option>
						<option value="Skill-Building">Skill-Building</option>
						<option value="Speaker Series">Speaker Series</option>
						<option value="Showcase / Competition">Showcase / Competition</option>
						<option value="Showcase / Competition">Mentoring</option>
					</select>
				</div>
				<div class="form-group">
					<select name="jobtype" class="form-control" id="jobtype">
						<option value="jobtype" disabled selected>Type</option>
						<option value="All">All</option>
						<option value="Full-Time">Full-Time</option>
						<option value="Part-Time">Part-Time</option>
						<option value="Temporary">Temporary</option>
						<option value="Contract">Contract</option>
						<option value="Internship">Internship</option>
						<option value="Job Shadow">Job Shadow</option>
					</select>
				</div>
				<div class="form-group">
					<select name="paid" class="form-control" id="paid">
						<option value="paid" disabled selected>Compensation</option>
						<option value="1">Paid</option>
						<option value="0">Unpaid</option>
					</select>
				</div>
				<div class="form-group text-input">
					<input placeholder="Location" name="location" type="text" class="form-control" id="location">
				</div>
				<div class="form-group text-input">
					<input placeholder="Company" name="company" type="text" class="form-control" id="company">
				</div>
				<button id="submit" type="submit" class="btn btn-primary">Filter Jobs</button>
			</form>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-offset-1 col-lg-10">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div style="padding-left:15px; padding-right:15px;" class="listing-cards">
					<div class="listing-cards">
						<div class="row">
							<?php echo do_shortcode( '[wpv-view name="Jobs"]'); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-1"></div>
		</div>
	</div>
</div>
<div class="news-button">
	<div style="margin-top: 40px; margin-bottom: 40px;" class="col-md-12 text-center">
		<a href="<?php echo site_url(); ?>/add-job" class="btn-primary">
			Submit Job Posting
		</a>
	</div>
</div>
<script type="text/javascript">
$( document ).ready(function() {
$("a.wpv-filter-next-link, a.wpv-filter-previous-link").click(function() {
$("html, body").animate( {
scrollTop : 0
}, 1000);
});
<?php
$is_industry = get_query_var('industry', 'industry');
if ($is_industry != 'industry') {
?>
$('#industry').val("<?php echo $is_industry; ?>");
<?php }
if ($is_industry == 'industry') { ?>
$('#industry').val('industry');
<?php }
?>
<?php
$is_interests = get_query_var('interests', 'interests');
if ($is_interests != 'interests') {
?>
$('#interests').val("<?php echo $is_interests; ?>");
<?php }
if ($is_interests == 'interests') { ?>
$('#interests').val('interests');
<?php }
?>
<?php
$is_jobtype = get_query_var('jobtype', 'jobtype');
if ($is_interests != 'jobtype') {
?>
$('#jobtype').val("<?php echo $is_jobtype; ?>");
<?php }
if ($is_jobtype == 'jobtype') { ?>
$('#jobtype').val('jobtype');
<?php }
?>
<?php
$is_paid = get_query_var('paid', 'paid');
if ($is_paid != 'paid') {
?>
$('#paid').val("<?php echo $is_paid; ?>");
<?php }
if ($is_paid == 'paid') { ?>
$('#paid').val('paid');
<?php }
?>
<?php
$is_location = get_query_var('location', 'location');
if ($is_location != 'location') {
?>
$('#location').val("<?php echo $is_location; ?>");
<?php } ?>
<?php
$is_company = get_query_var('company', 'company');
if ($is_company != 'company') {
?>
$('#company').val("<?php echo $is_company; ?>");
<?php } ?>
});
</script>