<div class="news-listing" id="listing">
	<section class="banner">
		<div class="container">
			<div class="listing-header">
				<div class="home-heading">
					<h1><span class="highlight-opacity">NEWS</span></h1>
				</div>
				<div class="home-body">
					<p class="white">
						<span class="highlight-opacity">find the latest news</span>
					</p>
				</div>
			</div>
		</div>
	</section>
	<div class="row">
		<div id="volunteer-filters">
			<form style="margin-top: 20px" role="form">
				<div class="form-group">
					<select name="industry" class="form-control" id="industry">
						<option value="industry" disabled selected>Industry</option>
						<option value="All">All</option>
						<option value="General">General</option>
						<option value="Tech">Tech</option>
						<option value="Arts">Arts</option>
						<option value="Social Entrepreneurship">Social Entrepreneurship</option>
						<option value="Non-Profit">Non-Profit</option>
					</select>
				</div>
				<div class="form-group">
					<select name="interests" class="form-control" id="interests">
						<option value="interests" disabled selected>Interests</option>
						<option value="All">All</option>
						<option value="Networking">Networking</option>
						<option value="Leadership / Self-Awareness">Leadership / Self-Awareness</option>
						<option value="Skill-Building">Skill-Building</option>
						<option value="Speaker Series">Speaker Series</option>
						<option value="Showcase / Competition">Showcase / Competition</option>
						<option value="Showcase / Competition">Mentoring</option>
					</select>
				</div>
				<div class="checkbox">
					<label>
						<input name="curated" id="curated" type="checkbox" value="verified">Curated
					</label>
					<input style="visibility:hidden;" type="checkbox" id='notcurated' value='notverified' name='notcurated'>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-6" style="">
						<div class="form-group" style="">
							<input style="font-size: 14px;" placeholder="From" name="from_date"
							type="date" id="from_date">
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="form-group" style="">
							<input style="font-size: 14px;" placeholder="To" name="to_date" type="date" id="to_date">
							<!-- <i style="color: #fff" class="glyphicon glyphicon-calendar form-control-feedback"></i> -->
						</div>
					</div>
				</div>
				<button id="submit" type="submit" class="btn btn-primary">Filter News</button>
			</form>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-offset-1 col-lg-10">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div style="padding-left:15px; padding-right:15px;" class="listing-cards">
					<div class="row">
						<?php echo do_shortcode( '[wpv-view name="News"]'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-1"></div>
	</div>
</div>
<div class="news-button">
	<div style="margin-top: 40px; margin-bottom: 40px;" class="col-md-12 text-center">
		<?php
		if (is_user_logged_in() == true)
		{ ?>
		<a href="<?php echo site_url(); ?>/add-news" class="btn-primary">
			Submit News Posting
		</a>
		<?php
		}
		?>
		<?php
		if (is_user_logged_in() == false)
		{ ?>
		<a href="<?php echo site_url(); ?>/startup/join" class="btn-primary">
			Submit News Posting
		</a>
		<?php
		}
		?>
	</div>
</div>
<script type="text/javascript">
$( document ).ready(function() {
$("a.wpv-filter-next-link, a.wpv-filter-previous-link").click(function() {
$("html, body").animate( {
scrollTop : 0
}, 1000);
});
<?php
$is_industry = get_query_var('industry', 'industry');
if ($is_industry != 'industry') {
?>
$('#industry').val("<?php echo $is_industry; ?>");
<?php }
if ($is_industry == 'industry') { ?>
$('#industry').val('industry');
<?php }
?>
<?php
$is_interests = get_query_var('interests', 'interests');
if ($is_interests != 'interests') {
?>
$('#interests').val("<?php echo $is_interests; ?>");
<?php }
if ($is_interests == 'interests') { ?>
$('#interests').val('interests');
<?php }
?>
<?php
$is_curated = get_query_var('curated', 'notcurated');
if ($is_curated == 'notcurated') { ?>
$('input#curated').prop('checked', false);
$('input#notcurated').prop('checked', true);
<?php }
if ($is_curated == 'verified') { ?>
$('input#curated').prop('checked', true);
$('input#notcurated').prop('checked', false);
<?php }
?>
});
$('input#curated').click(function() {
var $this = $(this);
// $this will contain a reference to the checkbox
if ($this.is(':checked')) {
$('input#notcurated').prop('checked', false);
} else {
$('input#notcurated').prop('checked', true);
}
});
</script>