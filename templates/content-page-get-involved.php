<div class="row">
    <div class="col-md-12">
        <div id="volunteer-cta">
            <div class="volunteer-cta-text">
                <div class="home-heading">
                    <h1 ><span class="highlight-opacity">Get Involved</span></h1>
                </div>
                <div class="home-body">
                    <p><span class="highlight-opacity white">Equip the Next Generation to Create Opportunity</span></p>
                </div>
                <div class="form">
                    <?php echo do_shortcode('[gravityform id=12 title=false description=false ajax=true]'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">
            <div id="volunteer-about">
                <div class="volunteer-heading">
                    <h1><span class="highlight-navy"><?php echo get_field('intro_header'); ?></span></h1>
                </div>
                <div class="volunteer-body">
                    <h3 class="blue">ABOUT FUTURE FOUNDERS:</h3>
                    <div class="col-md-12">
                        <p class="large blue">We immerse the next generation of
                            makers and builders in experiences that inspire and
                            empower them to create their own opportunity.
                        </p>
                    </div>
                    <br>
                    <br>
                    <div id="volunteer-featured">
                        <h3 class="blue">HOW YOU HELP FULFILL THE VISION:</h3>
                        <div class="col-md-4 col-lg-4">
                            <div class="facilitate">
                                <h2 class="black">Be a Facilitator</h2>
                                <h3><a class="light-blue" href="<?php
                                echo the_permalink(); ?>/facilitate">GO TO OPPORTUNITIES</a></h3>
                                <h5 class="black light">Facilitating with Future Founders</h5>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4">
                            <div class="mentor">
                                <h2 class="black">Be a Mentor</h2>
                                <h3><a class="pink" href="<?php
                                echo the_permalink(); ?>/mentoring">GO TO OPPORTUNITIES</a></h3>
                                <h5 class="black light">Mentoring with Future Founders</h5>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4">
                            <div class="speak">
                                <h2 class="black">Be a Speaker</h2>
                                <h3><a class="purple" href="<?php
                                echo the_permalink(); ?>/speak">GO TO OPPORTUNITIES</a></h3>
                                <h5 class="black light">Speaking with Future Founders</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-1 col-lg-1"></div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-offset-1 col-lg-10">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div id="volunteer-about">
                    <div class="volunteer-heading">
                        <h1><span class="highlight-navy">Featured Opportunities</span></h1>
                    </div>
                </div>
                <div class="listing-cards">
                    <?php
                    function getJsonFeed($url) {
                    $json_file = file_get_contents($url);
                    $jobs = json_decode($json_file);
                    date_default_timezone_set('America/Chicago');
                    return $jobs;
                    }
                    
                    $jobs = getJsonFeed('https://futurefounders.secure.force.com/services/apexrest/jobs/.json');
                    $i = 0;
                    foreach ($jobs as $job) {
                    if ($i % 3 == 0) {
                    ?>
                    <div class="row">
                        <?php }
                        $grade_level = ($job->{'Grade_Level__c'});
                        $group_size = ($job->{'Group_Size__c'});
                        $volunteer_type = strtolower($job->{'Type__c'});
                        $volunteer_date_object = new DateTime($job->{'GW_Volunteers__First_Shift__c'});
                        $volunteer_date_object->setTimeZone(new DateTimeZone('America/Chicago'));
                        $volunteer_date = $volunteer_date_object->format("l, F d");
                        $volunteer_start_time = $volunteer_date_object->format("g:ia");
                        $volunteers_needed = $job->{'GW_Volunteers__Number_of_Volunteers_Still_Needed__c'};
                        $name = $job->{'Name'};
                        $id = $job->{'Id'};
                        $start_date = $volunteer_date_object->format("n/d/Y");
                        $location = $job->{'Location_Name__c'};
                        $shift_slots = (array)$job->{'GW_Volunteers__Volunteer_Job_Slots__r'}->{'records'};
                        $records = (array)$shift_slots[0]->{'End_Date_Time__c'};
                        $end_time_string = $records[0];
                        $end_time = new DateTime($end_time_string);
                        $end_time->setTimeZone(new DateTimeZone('America/Chicago'));
                        $final_end_time = $end_time->format("g:ia");
                        $show_on_site = $job->{'GW_Volunteers__Display_on_Website__c'};
                        if ($show_on_site == true) {
                        ?>
                        <div class="col-sm-12 col-md-4 col-lg-4">
                            <div class="listing-body">
                                <?php if ($volunteer_type == 'facilitate') { ?>
                                <div id="volunteer-title" class="border-light-blue">
                                    <h5 class="light blue"><span class="highlight-light-blue white"><?php echo $volunteer_type; ?></span>
                                    <span>&nbsp;&nbsp;<?php echo "#" . $grade_level . "&nbsp;&nbsp;#" . $group_size  ?></span>
                                    </h5>
                                </div>
                                <?php } if ($volunteer_type == 'mentor') { ?>
                                <div id="volunteer-title" class="border-pink">
                                    <h5 class="light blue"><span class="highlight-pink white"><?php echo $volunteer_type; ?></span>
                                    <span>&nbsp;&nbsp;<?php echo "#" . $grade_level . "&nbsp;&nbsp;#" . $group_size  ?></span>
                                </h5>                                </div>
                                <?php } if ($volunteer_type == 'speak') { ?>
                                <div id="volunteer-title" class="border-purple">
                                    <h5 class="light blue"><span class="highlight-purple white"><?php echo $volunteer_type; ?></span>
                                    <span>&nbsp;&nbsp;<?php echo "#" . $grade_level . "&nbsp;&nbsp;#" . $group_size  ?></span>
                                    </h5>
                                </div>
                                <?php } ?>
                                <div  style="white-space:nowrap;" class="row">
                                    <div style="white-space:nowrap;" class="col-xs-7 col-sm-7 col-md-7 col-lg-7" id="date-text">
                                        <h5 class="blue"><?php echo $volunteer_date ?></h5>
                                    </div>
                                    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5" id="remaining-text">
                                        <?php if ($volunteers_needed > 1) { ?>
                                        <h5 class="blue light"><b><?php echo $volunteers_needed ?></b> <span style="font-weight: 300">SPOTS REMAIN</span></h5>
                                        <?php } ?>
                                        <?php if ($volunteers_needed == 1) { ?>
                                        <h5 class="blue light"><b><?php echo $volunteers_needed ?></b> <span style="font-weight: 300">SPOT REMAINS</span></h5>
                                        <?php } ?>
                                        <?php if ($volunteers_needed == 0) { ?>
                                        <h5 class="blue light">0 <span style="font-weight: 300">SPOTS REMAIN</span></h5>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div id="time-text">
                                    <h4 class="blue light"><?php echo $volunteer_start_time ?> - <?php echo $final_end_time ?></h4>
                                </div>
                                <h3 class="blue light name-text"><?php echo $name?></h3>
                                <div class="row">
                                    <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" id="location-text">
                                        <h5 class="blue light"><?php echo $location ?></h5>
                                    </div>
                                    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 text-right" id="register-text">
                                        <?php if ($volunteer_type == 'facilitate') { ?>
                                        <a class="light-blue" href="
                                            <?php
                                            $arr_params = array('volunteer_name' => $name, 'volunteer_date' => $volunteer_date, 'volunteer_start_time' => $volunteer_start_time, 'location' => $location, 'volunteer_job' => $id);
                                            echo esc_url(add_query_arg($arr_params, get_permalink('5525')));
                                            ?>">
                                            register now >
                                        </a>
                                        <?php } if ($volunteer_type == 'mentor') { ?>
                                        <a class="pink" href="
                                            <?php
                                            $arr_params = array('volunteer_name' => $name, 'volunteer_date' => $volunteer_date, 'volunteer_start_time' => $volunteer_start_time, 'location' => $location, 'volunteer_job' => $id);
                                            echo esc_url(add_query_arg($arr_params, get_permalink('5525')));
                                            ?>">
                                            register now >
                                        </a>
                                        <?php } if ($volunteer_type == 'speak') { ?>
                                        <a class="purple" href="
                                            <?php
                                            $arr_params = array('volunteer_name' => $name, 'volunteer_date' => $volunteer_date, 'volunteer_start_time' => $volunteer_start_time, 'location' => $location, 'volunteer_job' => $id);
                                            echo esc_url(add_query_arg($arr_params, get_permalink('5525')));
                                            ?>">
                                            register now >
                                        </a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        $i++;
                        if ($i % 3 == 0)
                        {
                        ?>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                    }
                    }
                    } ?>
                </div>
            </div>
            <div class="col-lg-1"></div>
        </div>
        <style type="text/css">
        #volunteer-featured h2 {
        font-size: 36px;
        }
        .featured-img img {
        width: 100%;
        height: 100%;
        max-width: 150px;
        max-height: 150px;
        }
        </style>