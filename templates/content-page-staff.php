<div id="staff">
	<?php if (is_page('Our Team')) { ?>
	<div class="row">
		<div class="col-md-12 text-center">
			<h1 class="blue">FUTURE FOUNDERS STAFF</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 text-center">
			<?php echo do_shortcode( '[wpv-view name="Staff"]'); ?>
		</div>
	</div>
	<div style="margin-top:40px;" class="row">
		<div class="col-md-12 text-center">
			<h2 style="margin-bottom:40px;"class="blue">BOARD OF DIRECTORS</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 text-center">
			<?php echo do_shortcode( '[wpv-view name="Board of Directors"]'); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 text-center">
			<h2 style="margin-top:40px; margin-bottom:40px;"class="blue">COMMUNITY ADVISORY BOARD</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 text-center">
			<div id="community-advisory">
				<?php echo do_shortcode( '[wpv-view name="Community Advisory Board"]'); ?>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<style type="text/css">
h3 {
	line-height: 24px;
}
</style>