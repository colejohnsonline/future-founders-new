<div style="margin-top: 75px;" id="staff">
	<?php if (is_page('Sponsors')) { ?>
	<div class="row">
		<div class="col-md-12 text-center">
			<h1 class="blue">PRESENTING SPONSORS</h1>
		</div>
	</div>
	<div class="row" style="margin-bottom:80px;">
		<div class="col-md-8 col-md-offset-2 text-center">
			<?php echo do_shortcode( '[wpv-view name="Presenting Sponsors"]'); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 text-center">
			<h2 class="blue">PREMIER SPONSORS</h2>
		</div>
	</div>
	<div class="row" style="margin-bottom:80px;">
		<div class="col-md-8 col-md-offset-2 text-center">
			<?php echo do_shortcode( '[wpv-view name="Premier Sponsors"]'); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 text-center">
			<h2 style="margin-bottom:40px;"class="blue">SPONSORS</h2>
		</div>
	</div>
	<div class="row" style="margin-bottom:80px;">
		<div class="col-md-8 col-md-offset-2 text-center">
			<?php echo do_shortcode( '[wpv-view name="Sponsors"]'); ?>
		</div>
	</div>
	<?php } ?>
	<?php if (is_page('Partners')) { ?>
	<div class="row">
		<div class="col-md-8 col-md-offset-2 text-center">
			<h1 class="blue">PARTNERS</h1>
			<?php echo do_shortcode( '[wpv-view name="Partners"]'); ?>
		</div>
	</div>
	<?php } ?>
	<?php if (is_page('School Partners')) { ?>
	<div class="row">
		<div class="col-md-8 col-md-offset-2 text-center">
			<h1 class="blue">SCHOOL PARTNERS</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 text-center">
			<?php echo do_shortcode( '[wpv-view name="School Partners"]'); ?>
		</div>
	</div>
	<?php } ?>
</div>