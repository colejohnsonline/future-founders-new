<?php if (!is_page(array( 'Sponsors', 'Partners', 'Apply', 'Profile', 'User', 'Join' ))) { ?>
<?php global $post; ?>
<?php if (!is_page_template(array( 'page-startup.php', 'page-news.php', 'page-jobs.php', 'page-events.php', 'page-student-listing.php' ))) { ?>
<?php if (!is_singular(array('event','job','news-article'))) { ?>
<div class="hidden-xs hidden-sm col-md-12 col-lg-12" id="sponsors" style="border-top: 1px black solid; height: 200px;">
	<?php echo do_shortcode( '[wpv-view name="Sponsors Slider"]'); ?>
</div>
<div class="col-xs-12 col-sm-12 hidden-md hidden-lg" id="sponsors" style="border-top: 1px black solid; height: 200px;">
	<?php echo do_shortcode( '[wpv-view name="Sponsors Slider Mobile"]'); ?>
</div>
<?php } ?>
<?php } ?>
<?php } ?>

<?php if (is_page_template(array( 'page-startup.php', 'page-news.php', 'page-jobs.php', 'page-events.php', 'page-student-listing.php', 'page-profile.php' ))) { ?>
<div class="hidden-xs hidden-sm col-md-12 col-lg-12" id="sponsors" style="border-top: 1px black solid; height: 200px;">
	<?php echo do_shortcode( '[wpv-view name="Startup Sponsors Slider"]'); ?>
</div>
<div class="col-xs-12 col-sm-12 hidden-md hidden-lg" id="sponsors" style="border-top: 1px black solid; height: 200px;">
	<?php echo do_shortcode( '[wpv-view name="Startup Sponsors Slider Mobile"]'); ?>
</div>
<?php } ?>

<?php if(is_page(array( 'Apply', 'Profile', 'User', 'Join' ))) { ?>
<div class="hidden-xs hidden-sm col-md-12 col-lg-12" id="sponsors" style="border-top: 1px black solid; height: 200px;">
	<?php echo do_shortcode( '[wpv-view name="Startup Sponsors Slider"]'); ?>
</div>
<div class="col-xs-12 col-sm-12 hidden-md hidden-lg" id="sponsors" style="border-top: 1px black solid; height: 200px;">
	<?php echo do_shortcode( '[wpv-view name="Startup Sponsors Slider Mobile"]'); ?>
</div>
<?php } ?>

<?php if(is_singular(array('event','job','news-article'))) { ?>
<div class="hidden-xs hidden-sm col-md-12 col-lg-12" id="sponsors" style="border-top: 1px black solid; height: 200px;">
	<?php echo do_shortcode( '[wpv-view name="Startup Sponsors Slider"]'); ?>
</div>
<div class="col-xs-12 col-sm-12 hidden-md hidden-lg" id="sponsors" style="border-top: 1px black solid; height: 200px;">
	<?php echo do_shortcode( '[wpv-view name="Startup Sponsors Slider Mobile"]'); ?>
</div>
<?php } ?>

<footer class="content-info" role="contentinfo">
	<div class="container-fluid">
		<div class="row">
			<div id="logo">
				<img src="<?= get_template_directory_uri(); ?>/dist/images/FFF-Logo.png">
			</div>
			<div id="menu">
				<div class="col-md-12">
					<?php dynamic_sidebar('sidebar-footer'); ?>
				</div>
			</div>
			<div id="social">
				<div class="row">
					<div class="hidden-xs hidden-sm" id="regular">
						<div class="col-xs-12 col-sm-12 col-md-4">
						</div>
						<div style="margin-left: 80px" class="col-xs-12 col-sm-12 col-md-1">
							<a target="_blank" href="https://www.facebook.com/futurefounders"><img src="<?= get_template_directory_uri(); ?>/dist/images/facebook.png"></a>
						</div>
						<div style="margin-left: -55px" class="col-xs-12 col-sm-12 col-md-1">
							<a target="_blank" href="https://twitter.com/futurefounders"><img src="<?= get_template_directory_uri(); ?>/dist/images/twitter.png"></a>
						</div>
						<div style="margin-left: -55px" class="col-xs-12 col-sm-12 col-md-1">
							<a target="_blank" href="https://www.linkedin.com/company/future-founders-foundation"><img src="<?= get_template_directory_uri(); ?>/dist/images/in.png"></a>
						</div>
						<div style="margin-left: -50px" class="col-xs-12 col-sm-12 col-md-1">
							<a target="_blank" href="https://instagram.com/futurefounders"><img src="<?= get_template_directory_uri(); ?>/dist/images/instagram.png"></a>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4">
						</div>
					</div>
					<div class="hidden-md hidden-lg" id="mobile">
						<div class="col-xs-12 col-sm-12 col-md-4">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-1">
							<a target="_blank" style="padding:5px;" href="https://www.facebook.com/futurefounders"><img src="<?= get_template_directory_uri(); ?>/dist/images/facebook.png"></a>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-1">
							<a target="_blank" style="padding:5px;" href="https://twitter.com/futurefounders"><img src="<?= get_template_directory_uri(); ?>/dist/images/twitter.png"></a>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-1">
							<a target="_blank" style="padding:5px;" href="https://www.linkedin.com/company/future-founders-foundation"><img src="<?= get_template_directory_uri(); ?>/dist/images/in.png"></a>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-1">
							<a target="_blank" style="padding:5px;" href="https://instagram.com/futurefounders"><img src="<?= get_template_directory_uri(); ?>/dist/images/instagram.png"></a>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4">
						</div>
					</div>
				</div>
			</div>
			<div id="address">
				<div class="row">
					<div class="col-md-12">
						<p>Future Founders</p>
						<p>600 W. Chicago Ave., Suite 775</p>
						<p>Chicago, IL 60654</p>
					</div>
				</div>
			</div>
			<div id="copyright">
				<div class="row">
					<div class="col-md-12">
						<p class="light">&copy; 2012-<?php echo substr(date("Y"), 2,3); ?> Future Founders</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>