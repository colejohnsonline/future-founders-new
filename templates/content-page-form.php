<div class="row">
	<div class="col-md-12">
		<div class="container">
			<div id="content">
				<p><?php the_content(); ?></p>
			</div>
		</div>
	</div>
</div>
<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>