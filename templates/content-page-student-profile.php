<div id="student-profile">
	<div class="container-fluid nopadding">
		<div class="row">
			<div class="col-md-12">
				<div class="profile">
					<?php echo do_shortcode( '[ultimatemember form_id=5265]'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div id="home" style="margin-top:60px!important">
		<section class="sub-banner-featured">
			<div class="container-fluid">
				<div class="row">
					<div class="hidden-xs hidden-sm col-md-12 col-lg-offset-1 col-lg-10">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="volunteer-heading">
								<h1><span class="highlight-navy">FEATURED EVENTS</span></h1>
							</div>
							<div class="row">
								<?php echo do_shortcode( '[wpv-view name="Super Featured Events"]'); ?>
							</div>
						</div>
					</div>
					<div class="hidden-xs hidden-sm hidden-md col-lg-1"></div>
				</div>
			</div>
		</section>
		<hr class="hidden-md hidden-lg">
		<section class="startup-listing">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-offset-1 col-lg-10">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<?php echo do_shortcode( '[wpv-view name="Startup Featured News"]'); ?>
					</div>
				</div>
				<div class="hidden-xs hidden-sm hidden-md col-lg-1"></div>
			</div>
			
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-offset-1 col-lg-10">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<?php echo do_shortcode( '[wpv-view name="Startup Featured Events"]'); ?>
					</div>
				</div>
				<div class="hidden-xs hidden-sm hidden-md col-lg-1"></div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-offset-1 col-lg-10">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<?php echo do_shortcode( '[wpv-view name="Startup Featured Jobs"]'); ?>
					</div>
				</div>
				<div class="hidden-xs hidden-sm hidden-md col-lg-1"></div>
			</div>
		</section>
	</div>
</div>