<div class="ffevents-listing" id="listing">
	<section class="banner">
		<div class="container">
			<div class="listing-header">
				<div class="home-heading">
					<h1><span class="highlight-opacity">EVENTS</span></h1>
				</div>
				<div class="home-body">
					<p class="white">
						<span class="highlight-opacity">find awesome events</span>
					</p>
				</div>
			</div>
		</div>
	</section>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-offset-1 col-lg-10">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div style="padding-left:15px; padding-right:15px;" class="listing-cards">
					<div class="listing-cards">
						<!--
						<div id="filter-body">
										<h4><span class="highlight-navy">Filter Opportunities</span></h4>
										<table>
														<tr>
																		<td><h5 class="filter"><span class="highlight-gray">Filter</span></h5></td>
																		<td><h5 class="filter"><span class="highlight-gray">Filter</span></h5></td>
																		<td><h5><a href="#">clear all filters</a></h5></td>
														</tr>
										</table>
						</div>
						-->
						<div class="row">
							<?php echo do_shortcode( '[wpv-view name="FF Events"]'); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-1"></div>
		</div>
	</div>
</div>