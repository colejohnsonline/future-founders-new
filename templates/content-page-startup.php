<div id="home" class="startup">
	<div class="nopadding">
		<img class="startup-page-banner" src="<?= get_template_directory_uri(); ?>/dist/images/do_something_bold.jpg"/>
	</div>
	<section class="banner">
		<div class="container">
			<div class="row">
				<div class="col-md-12 nopadding">
					<div class="content">
						<div class="home-heading">
							<h1><span class="highlight-navy">E-Pass</span></h1>
						</div>
						<div class="home-body">
							<p class="white">
								<span class="highlight-navy">the all access pass for aspiring entrepreneurs</span>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="home-buttons">
				<div class="row">
					<div class="col-md-12">
						<?php echo do_shortcode('[ultimatemember_social_login id=204]'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="content-links">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">
					<div id="startup" class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
						<?php
						// create array of ids to filter general events from featured events
							$featured_events = array();
						?>
						<?php
						$args = array( 'numberposts' => 2, 'post_type' => 'news-article',
						'meta_query' => array(
						array(
							'key' => 'show_as_featured_news',
							'value' => '1',
							'compare' => '='
							),
						array(
							'key' => 'startup_news',
							'value' => '1',
							'compare' => '='
							))
						);
						$myposts = get_posts( $args );
						foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
						<?php $latest_post_id = $post->ID;
							$featured_news[] = $latest_post_id;
						?>
						<?php
							$fallback_thumbnail = get_template_directory_uri() . "/dist/images/featured_news.jpg";
						?>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-12">
							<div style="width:300px;height:300px;overflow:hidden;padding-top:20px;" class="startup-featured-news">
								<div style="padding: 0; height: 100%" class="col-md-12">
									<h3 style="position:absolute; top:-20px; left:10px; line-height: 34px;"><span style="padding-left:0; padding-right:0; box-shadow: 10px 0 0 rgba(7,45,73,.8), -10px 0 0 rgba(7,47,73,.8);" class="highlight-opacity"><a class="serif white" href="<?php echo get_the_permalink($latest_post_id); ?>"><?php echo get_the_title($latest_post_id); ?></a></span></h3>
									<?php if (has_post_thumbnail()) { ?>
									<?php the_post_thumbnail(array(500,500), array( 'class' => 'img-responsive' ) ); ?>
									<?php } else { ?>
									<img style="width: 40%; position:absolute; bottom:125px; right:0px;" class="img-responsive" src="<?= get_template_directory_uri(); ?>/dist/images/chevron.png"/>
									<img style="width: 100%; position:absolute; top:90px; left:0px;" class="img-responsive" src="<?php echo $fallback_thumbnail; ?>">
									<?php } ?>
								</div>
							</div>
						</div>
						
						<?php endforeach; ?>
						<?php
						wp_reset_postdata();
						?>
					</div>
					<div id="news" class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
						<div class="links-header">
							<h3 class="serif">News</h3>
						</div>
						<?php
						$args = array( 'numberposts' => 3, 'post_type' => 'news-article', 'exclude' => $featured_news,
						'meta_query' => array(
						array(
							'key' => 'show_on_startup_home_page',
							'value' => '1',
							'compare' => '='
							)
						)
						);
						$myposts = get_posts( $args );
						foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
						<?php $latest_post_id = $post->ID;
						?>
						<?php if (wp_get_attachment_url( get_post_thumbnail_id($latest_post_id))) {
						$latest_image = wp_get_attachment_url( get_post_thumbnail_id($latest_post_id));
							} else {
						$latest_image = get_template_directory_uri() . "/dist/images/latest-news.png";
							}
							$fallback_thumbnail = get_template_directory_uri() . "/dist/images/latest-news.png";
						?>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 news-row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<div class="featured-img">
									<?php if (has_post_thumbnail()) { ?>
									<?php the_post_thumbnail('small'); ?>
									<?php } else { ?>
									<img class="img-responsive" style="max-width:300px;max-height:300px;" src="<?php echo $fallback_thumbnail; ?>">
									<?php } ?>
								</div>
							</div>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<div class="news-header">
									<a style="display: inline-block;" href="<?php the_permalink(); ?>"><?php echo $post->post_title; ?></a>
								</div>
							</div>
							<div class="clearfix"></div>
							</br>
						</div>
						<?php endforeach; ?>
						<?php
						wp_reset_postdata();
						?>
					</div>
					<div id="startup" class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
						<div class="links-header">
							<h3 class="serif">Startup</h3>
						</div>
						<div class="links">
							<h2>News</h2>
							<h4><a class="green" href="<?php the_permalink(); ?>/news">SEE MORE STARTUP NEWS ></a></h4>
							<a href="<?php the_permalink(); ?>/news"><p class="border-navy">learn about startup news</p></a>
							<br/>
							<h2>Events</h2>
							<h4><a class="other-blue" href="<?php the_permalink(); ?>/events">CHECK OUT MORE STARTUP EVENTS ></a></h4>
							<a href="<?php the_permalink(); ?>/events"><p class="border-navy">learn about startup events</p></a>
							<br/>
							<h2>Jobs</h2>
							<h4><a class="orange" href="<?php the_permalink(); ?>/jobs">BROWSE STARTUP JOBS ></a></h4>
							<p class="border-navy"><a href="<?php the_permalink(); ?>/jobs">learn about startup jobs</a></p>
						</div>
					</div>
				</div>
				<div class="col-md-1 col-lg-1"></div>
			</div>
		</div>
	</section>
	<section class="sub-banner-featured">
		<div class="container-fluid nopadding">
			<div class="row">
				<div style="margin-top: -35px;" class="col-xs-12 col-sm-12 col-md-12 col-lg-offset-1 col-lg-10">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="volunteer-heading">
							<h1><span class="highlight-navy">FEATURED EVENTS</span></h1>
						</div>
						<div class="row">
							<?php echo do_shortcode( '[wpv-view name="Super Featured Events"]'); ?>
						</div>
					</div>
				</div>
				<div class="col-md-1 col-lg-1"></div>
			</div>
		</div>
	</section>
	<hr class="hidden-md hidden-lg">
	<section class="startup-listing">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-offset-1 col-lg-10">
				<?php echo do_shortcode( '[wpv-view name="Startup Featured News"]'); ?>
			</div>
			<div class="col-md-1 col-lg-1"></div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-offset-1 col-lg-10">
				<?php echo do_shortcode( '[wpv-view name="Startup Featured Events"]'); ?>
			</div>
			<div class="col-md-1 col-lg-1"></div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-offset-1 col-lg-10">
				<?php echo do_shortcode( '[wpv-view name="Startup Featured Jobs"]'); ?>
			</div>
			<div class="col-md-1 col-lg-1"></div>
		</div>
	</section>
</div>
<style type="text/css">
div#um-shortcode-social-204 a.um-button.um-button-google {
padding: 4px 15px !important;
border-radius: 0!important;
font-size: 14px;
}
div#um-shortcode-social-204 a.um-button.um-button-linkedin {
padding: 4px 15px !important;
border-radius: 0 !important;
font-size: 14px;
}
</style>