<div class="container-static">
	<div id="volunteer-job-detail">
		<div class="content-blue">
			<div class="row">
				<?php if (!is_page('Donate')) { ?>
				<div class="hidden-xs hidden-sm hidden-md col-md-2">
					<div id="content">
					</div>
				</div>
				<div class="col-md-4">
					<div id="content">
						<p class="blue"><?php echo get_field('left_column') ?></p>
					</div>
				</div>
				<div class="col-md-4">
					<div id="content">
						<p class="blue"><?php echo get_field('right_column') ?></p>
					</div>
				</div>
				<div class="hidden-xs hidden-sm hidden-md col-md-2">
					<div id="content">
					</div>
				</div>
				<?php } ?>
				<?php if (is_page('Donate')) { ?>
				<div class="hidden-xs hidden-sm hidden-md col-md-2">
					<div id="content">
					</div>
				</div>
				<div class="col-md-6">
					<div id="content">
						<p class="blue"><?php echo get_field('left_column') ?></p>
					</div>
				</div>
				<div class="col-md-2">
					<div id="content">
						<p class="blue"><?php echo get_field('right_column') ?></p>
					</div>
				</div>
				<div class="hidden-xs hidden-sm hidden-md col-md-2">
					<div id="content">
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<div class="bullet-list">
		<div class="row">
			<div class="content-blue">
				<div class="hidden-xs hidden-sm hidden-md col-md-2">
					<div id="content">
					</div>
				</div>
				<div class="col-md-4">
					<p class="blue"><?php echo get_field('left_bullet_list') ?></p>
				</div>
				<div class="col-md-4">
					<p class="blue"><?php echo get_field('right_bullet_list') ?></p>
				</div>
				<div class="hidden-xs hidden-sm hidden-md col-md-2">
					<div id="content">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if (!is_page(array('Join', 'Apply'))) { ?>
<div class="hidden-sm hidden-xs detail-carousel">
	<div class="row">
		<div class="col-md-12">
			<?php echo do_shortcode( '[wpv-view name="FF News Slider"]'); ?>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-offset-1 col-lg-10">
		<div class="volunteer-heading">
			<h1><span class="highlight-navy">Featured Opportunities</span></h1>
		</div>
		<div class="listing-cards">
			<?php
			function getJsonFeed($url) {
			$json_file = file_get_contents($url);
			$jobs = json_decode($json_file);
			date_default_timezone_set('America/Chicago');
			return $jobs;
			}
			
			$jobs = getJsonFeed('https://futurefounders.secure.force.com/services/apexrest/jobs/.json');
			$i = 0;
			foreach ($jobs as $job) {
			if ($i % 3 == 0 && $i != 0) {?>
		</div>
		<div class="clearfix"></div>
		<?php
		break;
		?>
		
		<div class="row">
			
			<?php }
			$volunteer_type = strtolower($job->{'Type__c'});
			$volunteer_date_object = new DateTime($job->{'GW_Volunteers__First_Shift__c'});
			$volunteer_date_object->setTimeZone(new DateTimeZone('America/Chicago'));
			$volunteer_date = $volunteer_date_object->format("l, F d");
			$volunteer_start_time = $volunteer_date_object->format("g:ia");
			$volunteers_needed = $job->{'GW_Volunteers__Number_of_Volunteers_Still_Needed__c'};
			$name = $job->{'Name'};
			$id = $job->{'Id'};
			$start_date = $volunteer_date_object->format("n/d/Y");
			$location = $job->{'Location_Name__c'};
			$shift_slots = (array)$job->{'GW_Volunteers__Volunteer_Job_Slots__r'}->{'records'};
			$records = (array)$shift_slots[0]->{'End_Date_Time__c'};
			$end_time_string = $records[0];
			$end_time = new DateTime($end_time_string);
			$end_time->setTimeZone(new DateTimeZone('America/Chicago'));
			$final_end_time = $end_time->format("g:ia");
			?>
			
			
			<div class="col-sm-12 col-md-4 col-lg-4">
				<div class="listing-body">
					
					<?php if ($volunteer_type == 'facilitate') { ?>
					<div id="volunteer-title" class="border-light-blue">
						<h5 class="light"><span class="highlight-light-blue"><?php echo $volunteer_type; ?></span></h5>
					</div>
					<?php } if ($volunteer_type == 'mentor') { ?>
					<div id="volunteer-title" class="border-pink">
						<h5 class="light"><span class="highlight-pink"><?php echo $volunteer_type; ?></span></h5>
					</div>
					<?php } if ($volunteer_type == 'speak') { ?>
					<div id="volunteer-title" class="border-purple">
						<h5 class="light"><span class="highlight-purple"><?php echo $volunteer_type; ?></span></h5>
					</div>
					<?php } ?>
					<div class="row">
						<div style="white-space:nowrap;" class="col-xs-7 col-sm-7 col-md-7 col-lg-7" id="date-text">
							<h5 class="blue"><?php echo $volunteer_date ?></h5>
						</div>
						<div style="white-space:nowrap;" class="col-xs-5 col-sm-5 col-md-5 col-lg-5" id="remaining-text">
							<?php if ($volunteers_needed > 1) { ?>
							<h5 class="blue light"><b><?php echo $volunteers_needed ?></b> <span style="font-weight: 300">SPOTS REMAIN</span></h5>
							<?php } ?>
							<?php if ($volunteers_needed == 1) { ?>
							<h5 class="blue light"><b><?php echo $volunteers_needed ?></b> <span style="font-weight: 300">SPOT REMAINS</span></h5>
							<?php } ?>
							<?php if ($volunteers_needed == 0) { ?>
							<h5 class="blue light">0 <span style="font-weight: 300">SPOTS REMAIN</span></h5>
						<?php } ?>                            </div>
					</div>
					<div id="time-text">
						<h4 class="blue light"><?php echo $volunteer_start_time ?> - <?php echo $final_end_time ?></h4>
					</div>
					<h3 class="blue light name-text"><?php echo $name?></h3>
					<div class="row">
						<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" id="location-text">
							<h5 class="blue light"><?php echo $location ?></h5>
						</div>
						<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 text-right" id="register-text">
							<?php if ($volunteer_type == 'facilitate') { ?>
							<a class="light-blue" href="
								<?php
								$arr_params = array('volunteer_name' => $name, 'volunteer_date' => $volunteer_date, 'volunteer_start_time' => $volunteer_start_time, 'location' => $location, 'volunteer_job' => $id);
								echo esc_url(add_query_arg($arr_params, get_permalink('5525')));
								?>">
								register now >
							</a>
							<?php } if ($volunteer_type == 'mentor') { ?>
							<a class="pink" href="
								<?php
								$arr_params = array('volunteer_name' => $name, 'volunteer_date' => $volunteer_date, 'volunteer_start_time' => $volunteer_start_time, 'location' => $location, 'volunteer_job' => $id);
								echo esc_url(add_query_arg($arr_params, get_permalink('5525')));
								?>">
								register now >
							</a>
							<?php } if ($volunteer_type == 'speak') { ?>
							<a class="purple" href="
								<?php
								$arr_params = array('volunteer_name' => $name, 'volunteer_date' => $volunteer_date, 'volunteer_start_time' => $volunteer_start_time, 'location' => $location, 'volunteer_job' => $id);
								echo esc_url(add_query_arg($arr_params, get_permalink('5525')));
								?>">
								register now >
							</a>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<?php
			$i++;
			if ($i % 3 == 0)
			{
			?>
		</div>
		<div class="clearfix"></div>
		
		<?php
		}
		} ?>
	</div>
	<div class="col-lg-1"></div>
</div>
<?php } ?>
<?php if (is_page(array('Join', 'Apply'))) { ?>
<div class="hidden-sm hidden-xs detail-carousel">
	<div class="row">
		<div class="col-md-12">
			<?php echo do_shortcode( '[wpv-view name="Startup News Slider"]'); ?>
		</div>
	</div>
</div>
<section class="sub-banner-featured">
	<div class="container-fluid nopadding">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-offset-1 col-lg-10">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="volunteer-heading">
						<h1><span class="highlight-navy">FEATURED EVENTS</span></h1>
					</div>
					<div class="listing-cards">
						<div class="row">
							<?php echo do_shortcode( '[wpv-view name="Super Featured Events"]'); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-1 col-lg-1"></div>
		</div>
	</div>
</section>
<?php } ?>
<?php if (is_page('Donate')) { ?>
<style type="text/css">
.ginput_container {
width: 100%;
}
ul.gfield_radio label {
font-size: 10px;
}
.gfield_radio li input[type=radio] {
margin-left: 5px;
}
</style>
<?php
}
?>
<style type="text/css">
.sumome-share-client-wrapper.sumome-share-client-wrapper-left-page {
top: 300px !important;
}
div:empty {
display: none;
}
label {
text-transform: none!important;
}
.highlight-navy {
padding: 6px 8px;
}
h4 {
line-height: 24px;
}
</style>