<div class="row">
	<div class="col-md-12">
		<div id="content">
			<p><?php the_content(); ?></p>
		</div>
	</div>
</div>
<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>

<style type="text/css">
#gform_confirmation_message_2 {
	margin-top: 100px;
}
</style>