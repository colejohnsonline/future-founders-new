<div class="volunteer-listing" id="listing">
    <section class="banner">
        <div class="container">
            <div class="listing-header">
                <div class="home-heading">
                    <h1><span class="highlight-opacity">VOLUNTEER</span></h1>
                </div>
                <div class="home-body">
                    <p class="white">
                        <span class="highlight-opacity">equip the next generation to create opportunity</span>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <div class="row" style="margin-top: 30px;">
        <div>
            <div id="volunteer-filters">
                <form role="form">
                    <div class="form-group styled=select">
                        <select name="type" class="form-control" id="type">
                            <option value="opportunitytype" disabled selected>Opportunity Type</option>
                            <option value="all">All</option>
                            <option value="mentor">Mentor</option>
                            <option value="speak">Speak</option>
                            <option value="facilitate">Facilitate</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="grade_level" class="form-control" id="grade_level">
                            <option value="agegroup" disabled selected>Age Group</option>
                            <option value="all">All</option>
                            <option value="middleschool">Middle School</option>
                            <option value="highschool">High School</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="session" class="form-control" id="session">
                            <option value="sessiontype" disabled selected>Session Type</option>
                            <option value="all">All</option>
                            <option value="1/group">1/group</option>
                            <option value="1/class">1/class</option>
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-6" style="">
                            <div class="form-group" style="">
                                <input style="font-size: 14px;" placeholder="From" name="from_date"
                                type="date" id="from_date">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group" style="">
                                <input style="font-size: 14px;" placeholder="To" name="to_date" type="date" id="to_date">
                                <!-- <i style="color: #fff" class="glyphicon glyphicon-calendar form-control-feedback"></i> -->
                            </div>
                        </div>
                    </div>
                    <button id="submit" type="submit" class="btn btn-primary">Filter Opportunities</button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
include_once ('jobs-template.php'); ?>
<script type="text/javascript">
$( document ).ready(function() {
<?php
$is_type = get_query_var('type', 'opportunitytype');
if ($is_type != 'opportunitytype') {
?>
$('#type').val("<?php echo $param_type; ?>");
<?php }
if ($is_type == 'opportunitytype') { ?>
$('#type').val("opportunitytype");
<?php }
?>

<?php
$is_grade = get_query_var('grade_level', 'agegroup');
if ($is_grade != 'agegroup') {
?>
$('#grade_level').val("<?php echo $param_age; ?>");
<?php }
if ($is_grade == 'agegroup') { ?>
$('#grade_level').val("agegroup");
<?php }
?>

<?php
$is_session = get_query_var('session', 'sessiontype');
if ($is_session != 'sessiontype') {
?>
$('#session').val("<?php echo $is_session; ?>");
<?php }
if ($is_session == 'sessiontype') { ?>
$('#session').val("sessiontype");
<?php }
?>

<?php
$is_from = get_query_var('from_date', 'from');
if ($is_from != 'from') {
?>
$('#from_date').val("<?php echo $is_from; ?>");
<?php }
if ($is_from == 'from') { ?>
$('#from_date').val("<?php echo date('Y-m-d'); ?>");
<?php }
?>

<?php
$is_to = get_query_var('to_date', 'to');
if ($is_from != 'to') {
?>
$('#to_date').val("<?php echo $is_to; ?>");
<?php }
if ($is_to == 'to') { ?>
$('#to_date').val("<?php echo date('Y-m-d', mktime(0, 0, 0, 12, 31)); ?>");
<?php }
?>

/*
$(function() {
$( "#datepicker" ).datepicker();
});
$('form').submit(function(event) {
var formData = {
'type'              : $('#type').val(),
'grade_level'       : $('#grade_level').val(),
'session'    : $('#session').val(),
'from_date'    : $('#from_date').val(),
'to_date'    : $('#to_date').val()
};
$.ajax({
type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
url         : 'jobs-template.php', // the url where we want to POST
data        : formData, // our data object
dataType    : 'json', // what type of data do we expect back from the server
encode          : true
})
.done(function(data) {
console.log(data);
});
event.preventDefault();
});
*/
});
</script>