<?php while (have_posts()) : the_post(); ?>
<div class="container-fluid" style="margin-top:80px;">
	<article <?php post_class(); ?>>
		<header class="hidden-xs">
			<h1 style="white-space:pre-wrap; display: inline;" class="white"><span class="highlight-navy"><?php the_title(); ?></span></h1>
		</header>
		<header class="hidden-sm hidden-md hidden-lg">
			<h1 style="white-space:pre-wrap; display: inline;" class="blue"><span><?php the_title(); ?></span></h1>
		</header>
		<div id="volunteer-job-detail">
			<div class="content-blue container-fluid">
				<div class="row">
					<div class="col-md-2">
						<div id="content">
							<div class="stamp">
								<div class="hidden-xs hidden-sm col-md-2 col-lg-2">
									<div style="margin-top:-10px;"id="content">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div id="tags">
							<?php
							$count = 0;
							$posttags = get_the_tags();
							if ($posttags) {
							foreach($posttags as $tag) {
							$count++;
							if ($count <= 3 ) {
							echo '<a style="font-size: 14px;" href="' . get_tag_link($tag->term_id) . '">' . '#' . $tag->name . '&nbsp;&nbsp;' . '</a> ';
							}
							}
							}
							?>
						</div>
					</div>
				</div>
				<div class-"row">
					<div class="hidden-x hidden-sm col-md-2 col-lg-2"></div>
					<div class="col-md-4">
						<div id="content">
							<p class="blue"><?php echo get_field('left_column') ?></p>
						</div>
					</div>
					<div class="col-md-4">
						<div id="content" class="job-details">
							<?php the_post_thumbnail( $size, $attr ); ?>
							<?php if (in_category('Staff Members')) { ?>
							<h3 class="blue"><?php the_title(); ?></h3>
							<?php if (get_field('title')) { ?>
							<h3 class="light blue"><?php echo get_field('title') ?></h3>
							<?php } ?>
							<?php if (get_field('email')) { ?>
							<a href="mailto:<?php echo get_field('email') ?>"><h3 class="light blue">Email</h3></a>
							<? } ?>
							<?php if (get_field('phone')) { ?>
							<h3 class="light blue"><?php echo get_field('phone') ?></h3>
							<? } ?>
							<div class="col-md-12 nopadding" id="staff-social" style="margin-top:22px !important;">
								<?php if (get_field('linkedin')) { ?>
								<a style="display: inline-block;" target="_blank" href="<?php echo get_field('linkedin') ?>">
									<img style="float:left;"width="22" src="http://futurefounders.com/wp-content/uploads/2015/09/linkedin_social.png">
								</a>
								<? } ?>								
								<?php if (get_field('twitter') && get_field('linkedin')) { ?>
								<a style="display: inline-block;" target="_blank" href="<?php echo get_field('twitter') ?>">
									<img style="float:left;" width="22" src="http://futurefounders.com/wp-content/uploads/2015/09/twitter_social.png">
								</a>
								<? } ?>
								<?php if (get_field('twitter') && !get_field('linkedin')) { ?>
								<a style="display: inline-block;" target="_blank" href="<?php echo get_field('twitter') ?>">
									<img style="float:left;"width="22" src="http://futurefounders.com/wp-content/uploads/2015/09/twitter_social.png">
								</a>
								<? } ?>
							</div>
							<?php } ?>
							<p class="blue"><?php echo get_field('right_column') ?></p>
						</div>
					</div>
					<div class="hidden-x hidden-sm col-md-2 col-lg-2"></div>
				</div>
			</div>
			<div class="bullet-list">
				<div class="row">
					<div class="content-blue">
						<div class="col-md-2">
						</div>
						<div class="col-md-5">
							<p class="blue"><?php echo get_field('left_bullet_list') ?></p>
						</div>
						<div class="col-md-5">
							<p class="blue"><?php echo get_field('right_bullet_list') ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer>
		<style type="text/css">
		.sumome-share-client-wrapper.sumome-share-client-wrapper-left-page {
		top: 340px!important;
		}
		p {
		line-height: 1.4;
		font-size:;
		}
		span {
		line-height: 1.4;
		}
		#content li {
		font-weight: 300;
		line-height: 1.4;
		}
		.job-details h3 {
		margin-bottom: -12px;
		}
		p a {
		text-decoration: underline !important;
		}
		a:hover {
		color: #00bce4;
		}
		</style>
		<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
	</footer>
	<?php comments_template('/templates/comments.php'); ?>
</article>
</div>
<?php endwhile; ?>