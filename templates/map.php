<!--START MAP -->

<a name="map"></a>
<form id="map" name="map" action="<?php if (is_page(4)) {?>/about/#map<?php } ?><?php if (is_page(2805)) {?>/get-involved/school-partnerships/#map<?php } ?>" method="post">
    <p>
        <select id="schoolType" name="schoolType" onchange="map.submit();">
            <option value="All">All Schools</option>
            <option value="LEMON" <?php if($_POST['schoolType'] == 'LEMON') {?>selected="true"<?php } ?>>Lemonade Day Chicago Schools</option>
            <option value="CTTF" <?php if($_POST['schoolType'] == 'CTTF') {?>selected="true"<?php } ?>>Connect to the Future Schools</option>
            <option value="FFF" <?php if($_POST['schoolType'] == 'FFF') {?>selected="true"<?php } ?>>Future Founders Schools</option>
        </select>       
    </p>
    
    <?php if($_POST['schoolType'] == 'FFF') { ?>
    <iframe src="http://maps.google.com/maps/ms?msa=0&amp;msid=214681826041559604503.0004bca64010ccaf9fefc&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=41.796912,-87.659225&amp;spn=0.255951,0.415421&amp;z=11&amp;output=embed" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" width="606" height="500"></iframe>
    <?php } elseif($_POST['schoolType'] == 'CTTF') { ?>
    <iframe src="http://maps.google.com/maps/ms?msa=0&amp;msid=214681826041559604503.0004bca615159cfb1a267&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=41.796912,-87.659225&amp;spn=0.255951,0.415421&amp;z=11&amp;output=embed" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" width="606" height="500"></iframe>
    <?php } elseif($_POST['schoolType'] == 'LEMON') { ?>
    <iframe src="http://maps.google.com/maps/ms?msa=0&msid=201080360010641730052.0004ce514b87ce299b7a1&msa=0&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=41.796912,-87.659225&amp;spn=0.255951,0.415421&amp;z=11&amp;output=embed" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" width="606" height="500"></iframe>
    <?php } else { ?>
    <iframe src="https://maps.google.com/maps/ms?msid=207810331716204884576.0004d13639aa37a474b98&msa=0&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=41.796912,-87.659225&amp;spn=0.255951,0.415421&amp;z=11&amp;output=embed" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" width="606" height="500"></iframe>
    <?php } ?>
    <table>
        <tr>
            <td><p id="map-text"><img src="/wp-content/themes/futurefounders/images/yellow.png" />Lemonade Day Chicago</p></td>
            <td><p id="map-text"><img src="/wp-content/themes/futurefounders/images/blue.png" />Connect to the Future</p></td>
            <td><p id="map-text"><img src="/wp-content/themes/futurefounders/images/red.png" />Future Founders</p></td>
            <td><p id="map-text"><img src="/wp-content/themes/futurefounders/images/purple.png" />Multiple</p></td>
        </tr>
        
        
    </table>
</form>

<!--END MAP-->