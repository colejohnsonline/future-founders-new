<?php
if (is_user_logged_in() == true)
{
$user_id = um_profile_id();
$role = get_user_meta( $user_id, 'role', true );
if ($role != 'student' && !current_user_can('manage_options'))
{
header( 'Location: http://www.futurefounders.com/startup/join');
}
}
else
{
header( 'Location: http://www.futurefounders.com/startup/join');
}
?>
<?php while (have_posts()) : the_post(); ?>
<div id="detail-content" class="container-fluid">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div id="volunteer-job-detail">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <article <?php post_class(); ?>>
              <header class="hidden-xs">
                <h1 style="white-space:pre-wrap; display: inline;" class="white"><span class="highlight-navy"><?php the_title(); ?></span></h1>
              </header>
              <header class="hidden-sm hidden-md hidden-lg">
                <h1 style="white-space:pre-wrap; display: inline;" class="blue"><span><?php the_title(); ?></span></h1>
              </header>
            </article>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="tags">
              <?php 
              $industries = get_field("industry");
              foreach ($industries as $i) {
              ?>
              <a style="font-size: 14px;" target="_blank" href="<?php echo site_url(); ?>/startup/jobs/?industry=<?php echo $i; ?>"><?php echo '#' . $i ?></a>
              <?php } ?>
              <?php 
              $industries = get_field("interests");
              foreach ($industries as $i) {
              ?>
              <a style="font-size: 14px;" target="_blank" href="<?php echo site_url(); ?>/startup/jobs/?interest=<?php echo $i; ?>"><?php echo '#' . $i ?></a>
              <?php } ?>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="hidden-xs hidden-sm hidden-md col-md-1"></div>
          <div class="col-md-2 col-lg-2">
            <div id="stamp">
              <?php $curated = get_field('future_founders_verified');
              if ($curated == true) { ?>
              <h5 class="white">CURATED BY<br>FUTURE FOUNDERS</h5>
              <?php } ?>
            </div>
            <div id="author">
              <h5 class="author blue"><?php echo get_field('company_name'); ?></h5>
            </div>
            <div id="social" style="margin-top: 17px;">
              <h4 style="margin-bottom: 15px;" class="blue">Spread The Word!</h4>
              <?php echo do_shortcode( '[feather_share show="facebook,linkedin" hide="reddit, pinterest, twitter, google_plus, tumblr, mail"]' ); ?>
              <?php $arr_params = array('volunteer_job' => $id);
              ?>
              <a href="http://twitter.com/share?url=<?php echo get_permalink(); ?>;text=<?php echo the_title(); ?>;size=small&amp;count=none" target="_blank">
                <div>
                  <img style="width:35px; height:35px;" src="<?= get_template_directory_uri(); ?>/dist/images/twitter_social.png"/>
                </div>
              </a>
            </div>
          </div>
          <div class="col-md-4 col-lg-4">
            <div id="content">
              <h4 class="blue"><?php the_content(); ?></h4>
              <br/>
              <?php if (get_field('apply_link')) { ?>
              <?php if (strpos(get_field('apply_link'),'@') != false) { ?>
              <a class="btn-primary" target="_blank" href="mailto:<?php echo get_field('apply_link'); ?>">Apply Now</a>
              <?php } ?>
              <?php if (strpos(get_field('apply_link'),'@') != true) { ?>
              <a class="btn-primary" target="_blank" href="<?php echo get_field('apply_link'); ?>">Apply Now</a>
              <?php } ?>
              <?php } ?>
            </div>
          </div>
          <div class="col-md-4 col-lg-4">
            <div id="content" class="job-details">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="img-responsive"><?php the_post_thumbnail('medium', array('class' => 'alignleft')); ?></div>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="location-map img-responsive">
                    <iframe
                    width="450"
                    height="400"
                    frameborder="0" style="border:0"
                    src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDiVEy8GEh61VW5RG-oZ2xoMb1FyT51f-U&q=<?php echo get_field('address_line_1'), get_field('address_city'), get_field('address_state'), get_field('address_zip_code') ?>" allowfullscreen>
                    </iframe>
                  </div>
                  <h2 class="serif blue">Jobs Details</h2>
                  <div class="item">
                    <h3 class="light blue">Due:</h3>
                    <?php $date = DateTime::createFromFormat('Ymd', get_field('due'));?>
                    <h3 class="blue"><?php echo $date->format('F d, Y'); ?></h3>
                  </div>
                  <div class="item">
                    <h3 class="light blue">Company:</h3>
                    <h3 class="blue"><?php echo get_field('company_name'); ?></h3>
                  </div>
                  <div class="item">
                    <h3 class="light blue">Location:</h3>
                    <h3 class="blue">
                    <?php if (get_field('location_name') != "") { ?>
                    <?php echo get_field('location_name'); ?><br>
                    <?php } ?>
                    <?php if (get_field('address_line_1') != "") { ?>
                    <?php echo get_field('address_line_1'); ?><br>
                    <?php } ?>
                    <?php if (get_field('address_line_2') != "") { ?>
                    <?php echo get_field('address_line_2'); ?><br>
                    <?php } ?>
                    <?php echo get_field('address_city') . ", " . get_field('address_state') . " " . get_field('address_zip') ?>
                    </h3>
                  </div>
                  <div class="item">
                    <h3 class="light blue">Type:</h3>
                    <h3 class="blue"><?php echo get_field('type'); ?></h3>
                  </div>
                  <div class="item">
                    <h3 class="light blue">Compensated:</h3>
                    <?php $is_compensated = get_field('compensated'); ?>
                    <?php if ($is_compensated == true) { ?>
                    <h3 class="blue">Yes</h3>
                    <?php } else { ?>
                    <h3 class="blue">No</h3>
                    <?php } ?>
                  </div>
                  <div class="item">
                    <h3 class="light blue">Email:</h3>
                    <h3 class="blue"><a href="mailto:<?php echo get_field('email'); ?>"><?php echo get_field('email'); ?></a></h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="hidden-xs hidden-sm hidden-md col-md-1"></div>
        </div>
        <div class="row">
          <div class="col-md-2 col-lg-2"></div>
          <div style="margin-top:60px;" class="col-md-10 col-lg-10">
            <h4 class="blue"><a href="<?php echo site_url(); ?>/startup/jobs">< BACK TO JOBS</a></h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
  <style type="text/css">
  .sumome-share-client-wrapper.sumome-share-client-wrapper-left-page {
  top: 340px!important;
  }
  p {
  line-height: 1.4;
  font-size:;
  }
  span {
  line-height: 1.4;
  }
  #content li {
  font-weight: 300;
  line-height: 1.4;
  font-size: 16px;
  }
  .job-details h3 {
  margin-bottom: -12px;
  }
  p a {
  text-decoration: underline !important;
  }
  a:hover {
  color: #00bce4;
  }
  #detail-content img {
  width: 100%;
  height: 100%;
  }
  </style>
</footer>
<?php comments_template('/templates/comments.php'); ?>
</article>
</div>
<?php endwhile; ?>
<script>window.twttr = (function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0],
t = window.twttr || {};
if (d.getElementById(id)) return t;
js = d.createElement(s);
js.id = id;
js.src = "https://platform.twitter.com/widgets.js";
fjs.parentNode.insertBefore(js, fjs);
t._e = [];
t.ready = function(f) {
t._e.push(f);
};
return t;
}(document, "script", "twitter-wjs"));</script>