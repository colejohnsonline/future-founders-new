<?php
/**
 * Template Name: Edit Profile
 */
?>

<?php while (have_posts()) : the_post(); ?>
			<?php get_template_part('templates/page', 'profile-header'); ?>
			<?php get_template_part('templates/content', 'page-edit-profile'); ?>
<?php endwhile; ?>