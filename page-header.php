<?php use Roots\Sage\Titles; ?>

<?php if (!is_page( array( 'Get Involved', 'Home', 'News', 'Events', 'Jobs', 'Volunteer Detail', 'Staff', 'Sponsors', 'Board',
		'Partners', 'School Partners', 'Mentoring', 'Speak', 'Facilitate'))) { 
			if (!is_page_template('page-startup.php')) { ?>

<div class="page-header">
	<h1><span class="highlight-navy"><?= Titles\title(); ?></span></h1>
</div>

<?php } }?>