<?php get_template_part('templates/page', 'header'); ?>
<div class="col-sm-12 col-md-offset-2 col-md-8 col-md-offset-2 col-lg-offset-2 col-lg-8 col-lg-offset-2">
<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
<?php endwhile; ?>
</div>
<?php //the_posts_navigation(); ?>

<style type="text/css">
.comment-form .input-group-btn:last-child>input[type=submit], .input-group-btn:last-child>.btn, .input-group-btn:last-child>.btn-group {
	margin-top:0;
}
h2.entry-title {
	font-size: 24px;
	line-height: 30px;
}
</style>