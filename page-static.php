<?php
/**
 * Template Name: Static
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<div style="text-align: center;" class="container-static">
  <?php get_template_part('templates/page', 'header'); ?>
  </div>
  <?php get_template_part('templates/content', 'page-static'); ?>
  </div>
<?php endwhile; ?>
