<?php
/**
 * Template Name: Volunteer Listing
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php if(is_page('mentoring')) { ?>
  <?php get_template_part('templates/content', 'page-volunteer-mentoring'); ?>
  <?php } ?>
  <?php if(is_page('speak')) { ?>
  <?php get_template_part('templates/content', 'page-volunteer-speak'); ?>
  <?php } ?>
  <?php if(is_page('facilitate')) { ?>
  <?php get_template_part('templates/content', 'page-volunteer-facilitate'); ?>
  <?php } ?>
<?php endwhile; ?>