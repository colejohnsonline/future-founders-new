<?php
/**
 * Template Name: Volunteer Job
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page-volunteer-job'); ?>
<?php endwhile; ?>