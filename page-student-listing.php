<?php
/**
 * Template Name: Student Listing
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page-student-listing'); ?>
<?php endwhile; ?>
