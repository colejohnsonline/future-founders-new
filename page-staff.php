
<?php
/**
 * Template Name: Staff
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <div class="container-static">
  <?php get_template_part('templates/page', 'header'); ?>
  </div>
  <?php get_template_part('templates/content', 'page-staff'); ?>
<?php endwhile; ?>
