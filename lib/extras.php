<?php
namespace Roots\Sage\Extras;
use Roots\Sage\Config;
/**
* Add <body> classes
  */
  function body_class($classes) {
  
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
  if (!in_array(basename(get_permalink()), $classes)) {
  $classes[] = basename(get_permalink());
  }
  }
  
  // Add class if sidebar is active
  if (Config\display_sidebar()) {
  $classes[] = 'sidebar-primary';
  }
  return $classes;
  }
  add_filter('body_class', __NAMESPACE__ . '\\body_class');
  /**
  * Clean up the_excerpt()
  */
  function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
  }
  add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');
  /**
  * Register user types
  */
  function bbg_register_member_types_with_directory() {
  bp_register_member_type('student', array('labels' => array('name' => 'Students', 'singular_name' => 'Student',), 'has_directory' => 'custom-name'));
  }
  add_action('bp_register_member_types', 'bbg_register_member_types_with_directory');
  /* Custom Gravity Form Confirmations */
  /*
  function redirect_post($entry, $form) {
  $post_id = 5499;
  $post_name = get_permalink('$post_id');
  wp_redirect($post_name);
  }
  add_action('gform_after_submission', __NAMESPACE__, '\\redirect_post', 12, 2);
  add_filter( 'query_vars', 'add_my_variables' );
  function add_my_variables( $vars ){
  $vars[] = "start_date";
  $vars[] = "volunteer_job";
  return $vars;
  }
  */
  add_filter('init', function () {
  global $wp;
  $wp->add_query_var('start_date');
  $wp->add_query_var('volunteer_job');
  });
  /**
  * Add a widget to the dashboard.
  *
  * This function is hooked into the 'wp_dashboard_setup' action below.
  */
  add_action('wp_dashboard_setup', function () {
  wp_add_dashboard_widget('example_dashboard_widget',
  
  // Widget slug.
  'To post news, events, or jobs as an administrator:',
  
  // Title.
  function () {
  
  // Display whatever it is you want to show.
  
  
  ?>
  <a href="<?php
  echo home_url(); ?>/add-news">Add Startup News</a><br>
  <a href="<?php
  echo home_url(); ?>/add-event">Add Startup Event</a><br>
  <a href="<?php
  echo home_url(); ?>/add-job">Add Startup Job</a>
  <p>After submission you must approve under "All Items"</p>
  <br>
  <br>
  <!--
  <?php
  global $wpdb;
  $today = current_time('mysql', 1);
  $count = 20;
  if ($recentposts = $wpdb->get_results("SELECT ID, post_title FROM $wpdb->posts WHERE post_status = 'publish' AND post_modified_gmt < '$today' ORDER BY post_modified_gmt DESC LIMIT $count")):
  ?>
  <h2><?php
  _e("Recent Updates"); ?></h2>
  <ul>
    <?php
    foreach ($recentposts as $post) {
    if ($post->post_title == '') $post->post_title = sprintf(__('Post #%s'), $post->ID);
    echo "<li><a href='" . get_permalink($post->ID) . "'>";
      the_title();
    echo '</a></li>';
    }
    ?>
  </ul>
  <?php
  endif; ?>
  -->
  <?php
  }
  
  // Display function.
  );
  });
  // redirect gravity form add news job post
  add_filter('gform_confirmation', function ($confirmation, $form, $entry, $ajax) {
  if (current_user_can('manage_options')) {
  if ($form['id'] == '9') {
  //news
  $confirmation = array('redirect' => admin_url());
  }
  if ($form['id'] == '10') {
  //events
  $confirmation = array('redirect' => admin_url());
  }
  if ($form['id'] == '11') {
  //jobs
  $confirmation = array('redirect' => admin_url());
  }
  }
  return $confirmation;
  }, 10, 4);

  // views query for ff news
add_filter( 'wpv_filter_query', function ( $query_args ) {
    global $current_user;
    $types = (array) $query_args['post_type'];
    if ( !is_admin() && in_array( 'company', $types ) ) {
        $query_args['author'] = empty( $current_user->ID ) ? -1 : $current_user->ID;
    }
    return $query_args; 
  }, 99, 5709 );

 
add_action("gform_after_submission_14", function ($entry, $form)
{
   $post_id = $entry["post_id"];

   $date_value = get_post_custom_values("date", $post_id);
   update_field("field_55e07c6c07a49", $date_value, $post_id);

   $industry_values = get_post_custom_values("industry", $post_id);
   update_field("field_55e07eb8910f7", $industry_values, $post_id);
   $interest_values = get_post_custom_values("interests", $post_id);
   update_field("field_55e07f04910f8", $interest_values, $post_id);
}, 10, 2);


