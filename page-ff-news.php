<?php
/**
 * Template Name: Future Founders News
 */
?>
<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page-ff-news'); ?>
<?php endwhile; ?>
