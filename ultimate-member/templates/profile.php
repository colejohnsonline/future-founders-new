<div class="um <?php echo $this->get_class( $mode ); ?> um-<?php echo $form_id; ?>">
	<div class="um-form">
		
		<?php do_action('um_profile_before_header', $args ); ?>
		
		<?php if ( um_is_on_edit_profile() ) { ?><form method="post" action=""><?php } ?>
			
			<?php do_action('um_profile_header_cover_area', $args ); ?>
			
			<?php do_action('um_profile_header', $args ); ?>
			
			<?php // do_action('um_profile_navbar', $args ); ?>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?php
							
						$nav = $ultimatemember->profile->active_tab;
						$subnav = ( get_query_var('subnav') ) ? get_query_var('subnav') : 'default';
						
						print "<div class='um-profile-body $nav $nav-$subnav'>";
							// Custom hook to display tabbed content
							do_action("um_profile_content_{$nav}", $args);
							do_action("um_profile_content_{$nav}_{$subnav}", $args);
							?>
							<?php
						print "</div>";
						
						?>
					</div>
				</div>
			</div>
		<?php if ( um_is_on_edit_profile() ) { ?></form><?php } ?>
		
	</div>
</div>
<script type="text/javascript">
$( document ).ready(function() {
	$( "div.um-dropdown-b ul li:nth-child(2)" ).css("display", "none");
});
</script>
<style type-"text/css">
div.um-meta-text {
	display:none;
}

	/*Daniel Bazan, 10/20/15
	removes the grey lines of the 
	tables (bottom borders) from 
	the profile content*/
.um-profile.um-viewing .um-field-label {
	border: 0;
	margin-bottom: -10px
}

	/*Daniel Bazan, 10/20/15
	removes the grey lines of the 
	tables (bottom borders) from
	under the name/profile pic*/
.um-header {
	border: 0;
}

	/*Daniel Bazan, 10/20/15
	Increased font size from 15px
	to show that the user inputs are
	bigger than the labels on the 
	profile page
	*/
.um {
	font-size:25px;
}

.um-profile-photo a.um-profile-photo-img {
	/*Daniel Bazan, 10/20/15
	Shifted the positioning of
	profile picture to the left
	and up*/
	left:100px;
	top:-125px!important;

	/*Daniel Bazan, 10/23/15
	Adds border color to profile photo
	to match the user name headers. Also,
	profile photo is made smaller to match
	new design.
	*/
	background-color: #072D49!important;
	padding: 4px;

	/*Daniel Bazan, 10/23/15
	Decreased the profile photo size,
	should only affect desktop/uimob960
	*/
	width: 160px!important;
	height: 160px!important;
}

	/*Daniel Bazan, 10/23/15
	Decrease profile picture size
	on uimob800 devices
	*/
div.uimob800 .um-profile-photo a.um-profile-photo-img {
	width: 115px!important;
	height: 115px!important;
	top: -85px!important;
}

	/*Daniel Bazan, 10/20/15
	Shifted the positioning of
	profile picture to the center
	and lower on mobile versions
	*/
div.uimob500 .um-profile-photo {
	top:0px;
}
div.uimob340 .um-profile-photo {
	top:0px;
}

	/*Daniel Bazan, 10/21/15
	Lowered the cover photo
	positioning, users should
	be able to add a cover photo
	from mobile devices*/
.um-cover-e{
	margin-top: 80px
}

	/*Daniel Bazan, 10/21/15
	Moves the user first and last name
	to the right of the profile picture
	to match up with new design.
	*/
div.um-name {
	margin-top: -10px;
	margin-left: 40px;
}

	/*Daniel Bazan, 10/21/15
	Exceptions are in mobile devices.
	User first and last name will
	stay central and below the profile 
	picture on mobile devices.*/
div.uimob500 div.um-name {
	margin-top: 3pc!important;
	margin-left: 0!important;
}
div.uimob340 div.um-name {
	margin-top: 3pc!important;
	margin-left: 0!important;
}

	/*Daniel Bazan, 10/21/15
	Moved the options cog over
	for mobile devices, much easier
	to access that way
	*/
.um-profile-edit {
	top: 0pt;
}
div.uimob960 .um-profile-edit {
	top: 0pt;
}
div.uimob800 .um-profile-edit {
	top: 0pt;
}
div.uimob500 .um-profile-edit {
	top: 60pt!important;
	right: 45pt;
}
div.uimob340 .um-profile-edit {
	top: 60pt!important;
	right: 20pt;
}

	/*Daniel Bazan, 10/21/15
	Adds headers to each of the columns
	(Profile and Student Survey) to divide
	up the profile page. Same font as 
	user first and last names.
	*/
.um-col-121::before{
	content: "Profile";
	color: #072D49!important;
	text-transform: uppercase;
	font-family: Bitter,"Times New Roman",Times,serif;
	font-weight: 700;
}
.um-col-122::before{
	content: "Student Survey";
	color: #072D49!important;
	text-transform: uppercase;
	font-family: Bitter,"Times New Roman",Times,serif;
	font-weight: 700;
}

	/*Daniel Bazan, 10/23/15
	Increase font size of social
	media links; changes colors
	of media links to black
	*/
.um-field-label .um-field-label-icon{
	font-size: 40px;
	display: none; /*EDIT: 10/23/15 removes label icons*/
}
.um-5265.um .um-field-label {
	color: #000000!important;
}

	/*Daniel Bazan, 10/23/15
	Removes the rest of the
	labels for social media links
	*/
label[for="facebook-5265"] {
	/* display: none; */
}
label[for="twitter-5265"] {
	/* display: none; */
}
label[for="instagram-5265"] {
	/* display: none; */
}

.um-field-value {
	color: #072D49;
	font-weight: 300;
}

.um-field-checkbox-option {
	color: #072D49 !important;
	font-weight: 300 !important;
}

	/*Daniel Bazan, 10/23/15
	Replace social media links
	using images rather than words
	(example: facebook page link will
	now be the logo, not the url)
	
a[title="Facebook"]::before{
	content: "";
	width: 1px; height: 1px;
	/*background-image: url("../dist/images/facebook_social.png");*
	margin: 0 0 0 -210px;
}
a[title="Facebook"]::after{
	content: "\f082";
	width: 1px; height: 1px;
	/*background-image: url("../dist/images/facebook_social.png");*
	margin: 0 0 0 98px;
}
a[title="Twitter"]::before{
	content: "";
	width: 1px; height: 1px;
	/*background-image: url("../dist/images/twitter_social.png");*
	margin: 0 0 0 -210px;
}
a[title="Twitter"]::after{
	content: "\f099";
	width: 1px; height: 1px;
	/*background-image: url("../dist/images/twitter_social.png");*
	margin: 0 0 0 98px;
}
a[title="Instagram"]::before{
	content: "";
	width: 1px; height: 1px;
	/*background-image: url("../dist/images/instagram_social.png");*
	margin: 0 0 0 -210px;
}
a[title="Instagram"]::after{
	content: "\f16d";
	width: 1px; height: 1px;
	/*background-image: url("../dist/images/instagram_social.png");*
	margin: 0 0 0 98px;
}
</style>