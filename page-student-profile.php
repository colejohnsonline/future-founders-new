<?php
/**
 * Template Name: Student Profile
 */
?>

<?php while (have_posts()) : the_post(); ?>
			<?php get_template_part('templates/page', 'student-profile'); ?>
			<?php get_template_part('templates/content', 'page-student-profile'); ?>
<?php endwhile; ?>